Hack
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:HACK:TOUT
	single: CONFigure:WCDMa:SIGNaling<Instance>:HACK:HARQ
	single: CONFigure:WCDMa:SIGNaling<Instance>:HACK:REPetition
	single: CONFigure:WCDMa:SIGNaling<Instance>:HACK:MSFRames

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:HACK:TOUT
	CONFigure:WCDMa:SIGNaling<Instance>:HACK:HARQ
	CONFigure:WCDMa:SIGNaling<Instance>:HACK:REPetition
	CONFigure:WCDMa:SIGNaling<Instance>:HACK:MSFRames



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Hack.Hack
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.hack.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Hack_Smode.rst