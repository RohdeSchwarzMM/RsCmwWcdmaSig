Eagch
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STOP:WCDMa:SIGNaling<Instance>:EAGCh
	single: ABORt:WCDMa:SIGNaling<Instance>:EAGCh
	single: INITiate:WCDMa:SIGNaling<Instance>:EAGCh
	single: READ:WCDMa:SIGNaling<Instance>:EAGCh
	single: FETCh:WCDMa:SIGNaling<Instance>:EAGCh

.. code-block:: python

	STOP:WCDMa:SIGNaling<Instance>:EAGCh
	ABORt:WCDMa:SIGNaling<Instance>:EAGCh
	INITiate:WCDMa:SIGNaling<Instance>:EAGCh
	READ:WCDMa:SIGNaling<Instance>:EAGCh
	FETCh:WCDMa:SIGNaling<Instance>:EAGCh



.. autoclass:: RsCmwWcdmaSig.Implementations.Eagch.Eagch
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.eagch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Eagch_State.rst
	Eagch_Trace.rst