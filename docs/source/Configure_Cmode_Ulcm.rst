Ulcm
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CMODe:ULCM:TYPE

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CMODe:ULCM:TYPE



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cmode_.Ulcm.Ulcm
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cmode.ulcm.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cmode_Ulcm_Activation.rst