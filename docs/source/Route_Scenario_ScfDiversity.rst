ScfDiversity
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:WCDMa:SIGNaling<Instance>:SCENario:SCFDiversity:EXTernal
	single: ROUTe:WCDMa:SIGNaling<Instance>:SCENario:SCFDiversity:INTernal

.. code-block:: python

	ROUTe:WCDMa:SIGNaling<Instance>:SCENario:SCFDiversity:EXTernal
	ROUTe:WCDMa:SIGNaling<Instance>:SCENario:SCFDiversity:INTernal



.. autoclass:: RsCmwWcdmaSig.Implementations.Route_.Scenario_.ScfDiversity.ScfDiversity
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.scenario.scfDiversity.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Scenario_ScfDiversity_Flexible.rst