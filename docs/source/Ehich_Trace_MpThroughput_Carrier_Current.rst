Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:SIGNaling<Instance>:EHICh:TRACe:MPTHroughput:CARRier<Carrier>:CURRent
	single: FETCh:WCDMa:SIGNaling<Instance>:EHICh:TRACe:MPTHroughput:CARRier<Carrier>:CURRent

.. code-block:: python

	READ:WCDMa:SIGNaling<Instance>:EHICh:TRACe:MPTHroughput:CARRier<Carrier>:CURRent
	FETCh:WCDMa:SIGNaling<Instance>:EHICh:TRACe:MPTHroughput:CARRier<Carrier>:CURRent



.. autoclass:: RsCmwWcdmaSig.Implementations.Ehich_.Trace_.MpThroughput_.Carrier_.Current.Current
	:members:
	:undoc-members:
	:noindex: