Hsupa
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:GFACtor:HSUPa:EDPCch
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:GFACtor:HSUPa:DTTP
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:GFACtor:HSUPa:EDPFormula

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:UL:GFACtor:HSUPa:EDPCch
	CONFigure:WCDMa:SIGNaling<Instance>:UL:GFACtor:HSUPa:DTTP
	CONFigure:WCDMa:SIGNaling<Instance>:UL:GFACtor:HSUPa:EDPFormula



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Uplink_.Gfactor_.Hsupa.Hsupa
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.uplink.gfactor.hsupa.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Uplink_Gfactor_Hsupa_Etfci.rst