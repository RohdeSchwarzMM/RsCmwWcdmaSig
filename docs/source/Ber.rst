Ber
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate:WCDMa:SIGNaling<Instance>:BER
	single: FETCh:WCDMa:SIGNaling<Instance>:BER
	single: READ:WCDMa:SIGNaling<Instance>:BER
	single: STOP:WCDMa:SIGNaling<Instance>:BER
	single: ABORt:WCDMa:SIGNaling<Instance>:BER
	single: INITiate:WCDMa:SIGNaling<Instance>:BER

.. code-block:: python

	CALCulate:WCDMa:SIGNaling<Instance>:BER
	FETCh:WCDMa:SIGNaling<Instance>:BER
	READ:WCDMa:SIGNaling<Instance>:BER
	STOP:WCDMa:SIGNaling<Instance>:BER
	ABORt:WCDMa:SIGNaling<Instance>:BER
	INITiate:WCDMa:SIGNaling<Instance>:BER



.. autoclass:: RsCmwWcdmaSig.Implementations.Ber.Ber
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ber.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Ber_State.rst