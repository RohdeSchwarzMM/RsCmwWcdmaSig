Voice
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:VOICe:DTX
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:VOICe:SOURce
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:VOICe:CODec
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:VOICe:TFCI

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:VOICe:DTX
	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:VOICe:SOURce
	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:VOICe:CODec
	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:VOICe:TFCI



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Connection_.Voice.Voice
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.voice.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Voice_Delay.rst
	Configure_Connection_Voice_Amr.rst