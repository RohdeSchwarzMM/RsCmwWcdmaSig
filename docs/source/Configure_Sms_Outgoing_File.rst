File
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:SMS:OUTGoing:FILE:INFO
	single: CONFigure:WCDMa:SIGNaling<Instance>:SMS:OUTGoing:FILE

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:SMS:OUTGoing:FILE:INFO
	CONFigure:WCDMa:SIGNaling<Instance>:SMS:OUTGoing:FILE



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Sms_.Outgoing_.File.File
	:members:
	:undoc-members:
	:noindex: