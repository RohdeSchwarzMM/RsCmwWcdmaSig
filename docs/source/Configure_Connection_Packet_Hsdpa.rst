Hsdpa
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:PACKet:HSDPa:RWINdow
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:PACKet:HSDPa:TIMer

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:PACKet:HSDPa:RWINdow
	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:PACKet:HSDPa:TIMer



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Connection_.Packet_.Hsdpa.Hsdpa
	:members:
	:undoc-members:
	:noindex: