Sfn
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:ULLogging:SFN
	single: READ:WCDMa:SIGNaling<Instance>:ULLogging:SFN

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:ULLogging:SFN
	READ:WCDMa:SIGNaling<Instance>:ULLogging:SFN



.. autoclass:: RsCmwWcdmaSig.Implementations.UplinkLogging_.Sfn.Sfn
	:members:
	:undoc-members:
	:noindex: