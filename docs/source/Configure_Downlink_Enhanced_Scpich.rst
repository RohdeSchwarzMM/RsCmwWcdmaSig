Scpich
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:ENHanced:SCPich:PHASe
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:ENHanced:SCPich:SSCode

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:DL:ENHanced:SCPich:PHASe
	CONFigure:WCDMa:SIGNaling<Instance>:DL:ENHanced:SCPich:SSCode



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Downlink_.Enhanced_.Scpich.Scpich
	:members:
	:undoc-members:
	:noindex: