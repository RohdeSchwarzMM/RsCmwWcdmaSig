Uplink
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:UDEFined:FREQuency:UL:MINimum
	single: CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:UDEFined:FREQuency:UL:MAXimum

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:UDEFined:FREQuency:UL:MINimum
	CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:UDEFined:FREQuency:UL:MAXimum



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.RfSettings_.UserDefined_.Frequency_.Uplink.Uplink
	:members:
	:undoc-members:
	:noindex: