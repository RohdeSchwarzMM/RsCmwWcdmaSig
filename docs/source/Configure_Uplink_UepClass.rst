UepClass
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:UEPClass:REPorted
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:UEPClass:MANual

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:UL:UEPClass:REPorted
	CONFigure:WCDMa:SIGNaling<Instance>:UL:UEPClass:MANual



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Uplink_.UepClass.UepClass
	:members:
	:undoc-members:
	:noindex: