RepCaps
=========

Carrier (Global)
----------------------------------------------------

.. code-block:: python

	# Setting:
	driver.repcap_carrier_set(repcap.Carrier.C1)
	# Range:
	C1 .. C32
	# All values (32x):
	C1 | C2 | C3 | C4 | C5 | C6 | C7 | C8
	C9 | C10 | C11 | C12 | C13 | C14 | C15 | C16
	C17 | C18 | C19 | C20 | C21 | C22 | C23 | C24
	C25 | C26 | C27 | C28 | C29 | C30 | C31 | C32

Instance (Global)
----------------------------------------------------

.. code-block:: python

	# Setting:
	driver.repcap_instance_set(repcap.Instance.Inst1)
	# Range:
	Inst1 .. Inst16
	# All values (16x):
	Inst1 | Inst2 | Inst3 | Inst4 | Inst5 | Inst6 | Inst7 | Inst8
	Inst9 | Inst10 | Inst11 | Inst12 | Inst13 | Inst14 | Inst15 | Inst16

Band
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Band.B1
	# Range:
	B1 .. B32
	# All values (32x):
	B1 | B2 | B3 | B4 | B5 | B6 | B7 | B8
	B9 | B10 | B11 | B12 | B13 | B14 | B15 | B16
	B17 | B18 | B19 | B20 | B21 | B22 | B23 | B24
	B25 | B26 | B27 | B28 | B29 | B30 | B31 | B32

BandCombination
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.BandCombination.Nr1
	# Range:
	Nr1 .. Nr32
	# All values (32x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15 | Nr16
	Nr17 | Nr18 | Nr19 | Nr20 | Nr21 | Nr22 | Nr23 | Nr24
	Nr25 | Nr26 | Nr27 | Nr28 | Nr29 | Nr30 | Nr31 | Nr32

Cell
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Cell.Nr1
	# Range:
	Nr1 .. Nr16
	# All values (16x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15 | Nr16

CounterNo
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.CounterNo.Nr313
	# Values (1x):
	Nr313

Cycle
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Cycle.Nr1
	# Values (2x):
	Nr1 | Nr2

DownCarrier
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.DownCarrier.Dc1
	# Range:
	Dc1 .. Dc32
	# All values (32x):
	Dc1 | Dc2 | Dc3 | Dc4 | Dc5 | Dc6 | Dc7 | Dc8
	Dc9 | Dc10 | Dc11 | Dc12 | Dc13 | Dc14 | Dc15 | Dc16
	Dc17 | Dc18 | Dc19 | Dc20 | Dc21 | Dc22 | Dc23 | Dc24
	Dc25 | Dc26 | Dc27 | Dc28 | Dc29 | Dc30 | Dc31 | Dc32

HSSCch
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.HSSCch.No1
	# Values (4x):
	No1 | No2 | No3 | No4

IPversion
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.IPversion.IPv4
	# Values (2x):
	IPv4 | IPv6

NonContigCell
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.NonContigCell.Nc2
	# Values (3x):
	Nc2 | Nc3 | Nc4

PacketData
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.PacketData.Pd8
	# Range:
	Pd8 .. Pd384
	# All values (6x):
	Pd8 | Pd16 | Pd32 | Pd64 | Pd128 | Pd384

QuadratureAM
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.QuadratureAM.QAM16
	# Values (2x):
	QAM16 | QAM64

RefMeasChannel
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.RefMeasChannel.Ch1
	# Range:
	Ch1 .. Ch5
	# All values (5x):
	Ch1 | Ch2 | Ch3 | Ch4 | Ch5

SecondCode
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.SecondCode.Sc1
	# Values (4x):
	Sc1 | Sc2 | Sc3 | Sc4

Timer
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Timer.T313
	# Values (4x):
	T313 | T323 | T3212 | T3312

TransportBlock
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.TransportBlock.TBl1
	# Values (4x):
	TBl1 | TBl2 | TBl3 | TBl4

TransTimeInterval
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.TransTimeInterval.Tti2
	# Values (2x):
	Tti2 | Tti10

