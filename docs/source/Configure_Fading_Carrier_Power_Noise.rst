Noise
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:FADing:CARRier<Carrier>:POWer:NOISe:TOTal
	single: CONFigure:WCDMa:SIGNaling<Instance>:FADing:CARRier<Carrier>:POWer:NOISe

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:FADing:CARRier<Carrier>:POWer:NOISe:TOTal
	CONFigure:WCDMa:SIGNaling<Instance>:FADing:CARRier<Carrier>:POWer:NOISe



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Fading_.Carrier_.Power_.Noise.Noise
	:members:
	:undoc-members:
	:noindex: