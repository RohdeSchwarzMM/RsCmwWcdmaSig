Cmode
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Sense_.UeCapability_.Measurement_.Cmode.Cmode
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ueCapability.measurement.cmode.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UeCapability_Measurement_Cmode_Wcdma.rst
	Sense_UeCapability_Measurement_Cmode_Gsm.rst
	Sense_UeCapability_Measurement_Cmode_Lte.rst