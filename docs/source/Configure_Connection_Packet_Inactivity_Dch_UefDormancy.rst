UefDormancy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:PACKet:INACtivity:DCH:UEFDormancy:ENABle
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:PACKet:INACtivity:DCH:UEFDormancy:TIMer
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:PACKet:INACtivity:DCH:UEFDormancy:DSTate

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:PACKet:INACtivity:DCH:UEFDormancy:ENABle
	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:PACKet:INACtivity:DCH:UEFDormancy:TIMer
	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:PACKet:INACtivity:DCH:UEFDormancy:DSTate



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Connection_.Packet_.Inactivity_.Dch_.UefDormancy.UefDormancy
	:members:
	:undoc-members:
	:noindex: