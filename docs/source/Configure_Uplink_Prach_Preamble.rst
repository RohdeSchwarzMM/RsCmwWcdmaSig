Preamble
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:PRACh:PREamble:AICH
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:PRACh:PREamble:SSIZe
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:PRACh:PREamble:SUBChannels
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:PRACh:PREamble:MCYCles
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:PRACh:PREamble:MRETrans
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:PRACh:PREamble:SIGNature

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:UL:PRACh:PREamble:AICH
	CONFigure:WCDMa:SIGNaling<Instance>:UL:PRACh:PREamble:SSIZe
	CONFigure:WCDMa:SIGNaling<Instance>:UL:PRACh:PREamble:SUBChannels
	CONFigure:WCDMa:SIGNaling<Instance>:UL:PRACh:PREamble:MCYCles
	CONFigure:WCDMa:SIGNaling<Instance>:UL:PRACh:PREamble:MRETrans
	CONFigure:WCDMa:SIGNaling<Instance>:UL:PRACh:PREamble:SIGNature



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Uplink_.Prach_.Preamble.Preamble
	:members:
	:undoc-members:
	:noindex: