Language
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CBS:MESSage:LANGuage

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CBS:MESSage:LANGuage



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cbs_.Message_.Language.Language
	:members:
	:undoc-members:
	:noindex: