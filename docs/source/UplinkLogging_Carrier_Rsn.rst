Rsn
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:ULLogging:CARRier<Carrier>:RSN
	single: READ:WCDMa:SIGNaling<Instance>:ULLogging:CARRier<Carrier>:RSN

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:ULLogging:CARRier<Carrier>:RSN
	READ:WCDMa:SIGNaling<Instance>:ULLogging:CARRier<Carrier>:RSN



.. autoclass:: RsCmwWcdmaSig.Implementations.UplinkLogging_.Carrier_.Rsn.Rsn
	:members:
	:undoc-members:
	:noindex: