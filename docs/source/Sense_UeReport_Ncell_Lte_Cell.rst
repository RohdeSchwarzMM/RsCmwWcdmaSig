Cell
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:WCDMa:SIGNaling<Instance>:UEReport:NCELl:LTE:CELL<Cell>

.. code-block:: python

	SENSe:WCDMa:SIGNaling<Instance>:UEReport:NCELl:LTE:CELL<Cell>



.. autoclass:: RsCmwWcdmaSig.Implementations.Sense_.UeReport_.Ncell_.Lte_.Cell.Cell
	:members:
	:undoc-members:
	:noindex: