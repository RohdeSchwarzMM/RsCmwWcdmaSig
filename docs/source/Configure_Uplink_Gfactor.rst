Gfactor
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:GFACtor:VIDeo
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:GFACtor:VOICe
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:GFACtor:HSDPa

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:UL:GFACtor:VIDeo
	CONFigure:WCDMa:SIGNaling<Instance>:UL:GFACtor:VOICe
	CONFigure:WCDMa:SIGNaling<Instance>:UL:GFACtor:HSDPa



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Uplink_.Gfactor.Gfactor
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.uplink.gfactor.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Uplink_Gfactor_Pdata.rst
	Configure_Uplink_Gfactor_Rmc.rst
	Configure_Uplink_Gfactor_Hsupa.rst