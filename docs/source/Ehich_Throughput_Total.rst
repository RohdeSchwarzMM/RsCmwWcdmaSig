Total
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:SIGNaling<Instance>:EHICh:THRoughput:TOTal
	single: FETCh:WCDMa:SIGNaling<Instance>:EHICh:THRoughput:TOTal

.. code-block:: python

	READ:WCDMa:SIGNaling<Instance>:EHICh:THRoughput:TOTal
	FETCh:WCDMa:SIGNaling<Instance>:EHICh:THRoughput:TOTal



.. autoclass:: RsCmwWcdmaSig.Implementations.Ehich_.Throughput_.Total.Total
	:members:
	:undoc-members:
	:noindex: