Enhanced
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Sense_.Downlink_.Carrier_.Enhanced.Enhanced
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.downlink.carrier.enhanced.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Downlink_Carrier_Enhanced_Dpch.rst