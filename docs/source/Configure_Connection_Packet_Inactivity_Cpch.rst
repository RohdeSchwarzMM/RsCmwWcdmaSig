Cpch
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:PACKet:INACtivity:CPCH:TIMer

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:PACKet:INACtivity:CPCH:TIMer



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Connection_.Packet_.Inactivity_.Cpch.Cpch
	:members:
	:undoc-members:
	:noindex: