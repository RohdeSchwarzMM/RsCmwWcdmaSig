UeReport
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CMODe:UEReport:ACTivation
	single: CONFigure:WCDMa:SIGNaling<Instance>:CMODe:UEReport:ENABle

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CMODe:UEReport:ACTivation
	CONFigure:WCDMa:SIGNaling<Instance>:CMODe:UEReport:ENABle



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cmode_.UeReport.UeReport
	:members:
	:undoc-members:
	:noindex: