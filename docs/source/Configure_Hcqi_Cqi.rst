Cqi
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:HCQI:CQI:MSFRames

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:HCQI:CQI:MSFRames



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Hcqi_.Cqi.Cqi
	:members:
	:undoc-members:
	:noindex: