Edc
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:CARRier<Carrier>:EDC:INPut
	single: CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:CARRier<Carrier>:EDC:OUTPut

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:CARRier<Carrier>:EDC:INPut
	CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:CARRier<Carrier>:EDC:OUTPut



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.RfSettings_.Carrier_.Edc.Edc
	:members:
	:undoc-members:
	:noindex: