Ergch
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STOP:WCDMa:SIGNaling<Instance>:ERGCh
	single: ABORt:WCDMa:SIGNaling<Instance>:ERGCh
	single: INITiate:WCDMa:SIGNaling<Instance>:ERGCh
	single: FETCh:WCDMa:SIGNaling<Instance>:ERGCh
	single: READ:WCDMa:SIGNaling<Instance>:ERGCh

.. code-block:: python

	STOP:WCDMa:SIGNaling<Instance>:ERGCh
	ABORt:WCDMa:SIGNaling<Instance>:ERGCh
	INITiate:WCDMa:SIGNaling<Instance>:ERGCh
	FETCh:WCDMa:SIGNaling<Instance>:ERGCh
	READ:WCDMa:SIGNaling<Instance>:ERGCh



.. autoclass:: RsCmwWcdmaSig.Implementations.Ergch.Ergch
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ergch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Ergch_State.rst