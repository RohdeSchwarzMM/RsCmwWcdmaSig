State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:CSWitched:STATe

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:CSWitched:STATe



.. autoclass:: RsCmwWcdmaSig.Implementations.Cswitched_.State.State
	:members:
	:undoc-members:
	:noindex: