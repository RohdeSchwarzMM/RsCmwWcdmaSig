RsCmwWcdmaSig API Structure
========================================


.. rubric:: Global RepCaps

.. code-block:: python
	
	driver = RsCmwWcdmaSig('TCPIP::192.168.2.101::HISLIP')
	# Carrier range: C1 .. C32
	rc = driver.repcap_carrier_get()
	driver.repcap_carrier_set(repcap.Carrier.C1)

[4SP]# Instance range: Inst1 .. Inst16
	rc = driver.repcap_instance_get()
	driver.repcap_instance_set(repcap.Instance.Inst1)

.. autoclass:: RsCmwWcdmaSig.RsCmwWcdmaSig
	:members:
	:undoc-members:
	:noindex:

.. rubric:: Subgroups

.. toctree::
	:maxdepth: 6
	:glob:

	Configure.rst
	Prepare.rst
	Sense.rst
	Clean.rst
	UeReport.rst
	Route.rst
	Rsignaling.rst
	Pswitched.rst
	Cswitched.rst
	Call.rst
	Source.rst
	Ber.rst
	Throughput.rst
	Hack.rst
	Hcqi.rst
	UplinkLogging.rst
	Eagch.rst
	Ehich.rst
	Ergch.rst