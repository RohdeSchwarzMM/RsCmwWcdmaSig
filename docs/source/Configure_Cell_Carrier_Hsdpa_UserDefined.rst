UserDefined
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:CARRier<Carrier>:HSDPa:UDEFined:ENABle
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:CARRier<Carrier>:HSDPa:UDEFined:MODulation
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:CARRier<Carrier>:HSDPa:UDEFined:NCODes
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:CARRier<Carrier>:HSDPa:UDEFined:TTI

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:CARRier<Carrier>:HSDPa:UDEFined:ENABle
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:CARRier<Carrier>:HSDPa:UDEFined:MODulation
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:CARRier<Carrier>:HSDPa:UDEFined:NCODes
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:CARRier<Carrier>:HSDPa:UDEFined:TTI



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Carrier_.Hsdpa_.UserDefined.UserDefined
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.carrier.hsdpa.userDefined.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cell_Carrier_Hsdpa_UserDefined_Tblock.rst