Dpcch
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:ULLogging:CARRier<Carrier>:DPCCh
	single: READ:WCDMa:SIGNaling<Instance>:ULLogging:CARRier<Carrier>:DPCCh

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:ULLogging:CARRier<Carrier>:DPCCh
	READ:WCDMa:SIGNaling<Instance>:ULLogging:CARRier<Carrier>:DPCCh



.. autoclass:: RsCmwWcdmaSig.Implementations.UplinkLogging_.Carrier_.Dpcch.Dpcch
	:members:
	:undoc-members:
	:noindex: