Hbit
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:ULLogging:CARRier<Carrier>:HBIT
	single: READ:WCDMa:SIGNaling<Instance>:ULLogging:CARRier<Carrier>:HBIT

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:ULLogging:CARRier<Carrier>:HBIT
	READ:WCDMa:SIGNaling<Instance>:ULLogging:CARRier<Carrier>:HBIT



.. autoclass:: RsCmwWcdmaSig.Implementations.UplinkLogging_.Carrier_.Hbit.Hbit
	:members:
	:undoc-members:
	:noindex: