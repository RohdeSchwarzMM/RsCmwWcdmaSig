Attempt
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CLEan:WCDMa:SIGNaling<Instance>:CONNection:CSWitched:ATTempt

.. code-block:: python

	CLEan:WCDMa:SIGNaling<Instance>:CONNection:CSWitched:ATTempt



.. autoclass:: RsCmwWcdmaSig.Implementations.Clean_.Connection_.Cswitched_.Attempt.Attempt
	:members:
	:undoc-members:
	:noindex: