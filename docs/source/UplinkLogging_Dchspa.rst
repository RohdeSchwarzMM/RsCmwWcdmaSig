Dchspa
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:ULLogging:DCHSpa
	single: READ:WCDMa:SIGNaling<Instance>:ULLogging:DCHSpa

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:ULLogging:DCHSpa
	READ:WCDMa:SIGNaling<Instance>:ULLogging:DCHSpa



.. autoclass:: RsCmwWcdmaSig.Implementations.UplinkLogging_.Dchspa.Dchspa
	:members:
	:undoc-members:
	:noindex: