Channel
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:CARRier<Carrier>:CHANnel:UL
	single: CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:CARRier<Carrier>:CHANnel:DL

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:CARRier<Carrier>:CHANnel:UL
	CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:CARRier<Carrier>:CHANnel:DL



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.RfSettings_.Carrier_.Channel.Channel
	:members:
	:undoc-members:
	:noindex: