UeCapability
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:WCDMa:SIGNaling<Instance>:UECapability:HSUPa
	single: SENSe:WCDMa:SIGNaling<Instance>:UECapability:HSDPa
	single: SENSe:WCDMa:SIGNaling<Instance>:UECapability:GENeral
	single: SENSe:WCDMa:SIGNaling<Instance>:UECapability:MRAT
	single: SENSe:WCDMa:SIGNaling<Instance>:UECapability:MMODe
	single: SENSe:WCDMa:SIGNaling<Instance>:UECapability:PUPLink
	single: SENSe:WCDMa:SIGNaling<Instance>:UECapability:PDOWnlink
	single: SENSe:WCDMa:SIGNaling<Instance>:UECapability:RLC
	single: SENSe:WCDMa:SIGNaling<Instance>:UECapability:PDCP
	single: SENSe:WCDMa:SIGNaling<Instance>:UECapability:IMSVoice

.. code-block:: python

	SENSe:WCDMa:SIGNaling<Instance>:UECapability:HSUPa
	SENSe:WCDMa:SIGNaling<Instance>:UECapability:HSDPa
	SENSe:WCDMa:SIGNaling<Instance>:UECapability:GENeral
	SENSe:WCDMa:SIGNaling<Instance>:UECapability:MRAT
	SENSe:WCDMa:SIGNaling<Instance>:UECapability:MMODe
	SENSe:WCDMa:SIGNaling<Instance>:UECapability:PUPLink
	SENSe:WCDMa:SIGNaling<Instance>:UECapability:PDOWnlink
	SENSe:WCDMa:SIGNaling<Instance>:UECapability:RLC
	SENSe:WCDMa:SIGNaling<Instance>:UECapability:PDCP
	SENSe:WCDMa:SIGNaling<Instance>:UECapability:IMSVoice



.. autoclass:: RsCmwWcdmaSig.Implementations.Sense_.UeCapability.UeCapability
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ueCapability.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UeCapability_Codec.rst
	Sense_UeCapability_Measurement.rst
	Sense_UeCapability_UePosition.rst
	Sense_UeCapability_RfParameter.rst