Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:THRoughput:TRACe:DL:SDU:AVERage
	single: READ:WCDMa:SIGNaling<Instance>:THRoughput:TRACe:DL:SDU:AVERage

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:THRoughput:TRACe:DL:SDU:AVERage
	READ:WCDMa:SIGNaling<Instance>:THRoughput:TRACe:DL:SDU:AVERage



.. autoclass:: RsCmwWcdmaSig.Implementations.Throughput_.Trace_.Downlink_.Sdu_.Average.Average
	:members:
	:undoc-members:
	:noindex: