Carrier
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Downlink_.Carrier.Carrier
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.downlink.carrier.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Downlink_Carrier_Ocns.rst
	Configure_Downlink_Carrier_Level.rst
	Configure_Downlink_Carrier_Code.rst
	Configure_Downlink_Carrier_Enhanced.rst
	Configure_Downlink_Carrier_Hsscch.rst