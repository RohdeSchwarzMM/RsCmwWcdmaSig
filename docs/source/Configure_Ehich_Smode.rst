Smode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:EHICh:SMODe:AVERage

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:EHICh:SMODe:AVERage



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Ehich_.Smode.Smode
	:members:
	:undoc-members:
	:noindex: