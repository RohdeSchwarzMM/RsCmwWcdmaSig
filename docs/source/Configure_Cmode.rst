Cmode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CMODe:PATTern

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CMODe:PATTern



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cmode.Cmode
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cmode.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cmode_Ulcm.rst
	Configure_Cmode_Single.rst
	Configure_Cmode_UeReport.rst