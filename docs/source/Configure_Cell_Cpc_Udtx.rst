Udtx
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:UDTX:ENABle
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:UDTX:LPLength
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:UDTX:CQITimer

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:UDTX:ENABle
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:UDTX:LPLength
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:UDTX:CQITimer



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Cpc_.Udtx.Udtx
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.cpc.udtx.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cell_Cpc_Udtx_Cycle.rst