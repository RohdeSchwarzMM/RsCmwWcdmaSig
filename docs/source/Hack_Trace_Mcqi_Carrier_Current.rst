Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:HACK:TRACe:MCQI:CARRier<Carrier>:CURRent
	single: READ:WCDMa:SIGNaling<Instance>:HACK:TRACe:MCQI:CARRier<Carrier>:CURRent

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:HACK:TRACe:MCQI:CARRier<Carrier>:CURRent
	READ:WCDMa:SIGNaling<Instance>:HACK:TRACe:MCQI:CARRier<Carrier>:CURRent



.. autoclass:: RsCmwWcdmaSig.Implementations.Hack_.Trace_.Mcqi_.Carrier_.Current.Current
	:members:
	:undoc-members:
	:noindex: