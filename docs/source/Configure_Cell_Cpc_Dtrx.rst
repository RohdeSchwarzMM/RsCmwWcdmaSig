Dtrx
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:DTRX:DELay
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:DTRX:OFFSet

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:DTRX:DELay
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:DTRX:OFFSet



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Cpc_.Dtrx.Dtrx
	:members:
	:undoc-members:
	:noindex: