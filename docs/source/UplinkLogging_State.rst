State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:ULLogging:STATe

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:ULLogging:STATe



.. autoclass:: RsCmwWcdmaSig.Implementations.UplinkLogging_.State.State
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uplinkLogging.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UplinkLogging_State_All.rst