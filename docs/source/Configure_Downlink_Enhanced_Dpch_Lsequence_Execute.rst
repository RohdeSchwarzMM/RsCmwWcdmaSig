Execute
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:ENHanced:DPCH:LSEQuence:EXECute

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:DL:ENHanced:DPCH:LSEQuence:EXECute



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Downlink_.Enhanced_.Dpch_.Lsequence_.Execute.Execute
	:members:
	:undoc-members:
	:noindex: