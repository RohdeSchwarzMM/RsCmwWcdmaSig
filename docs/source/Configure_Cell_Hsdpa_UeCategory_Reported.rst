Reported
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSDPa:UECategory:REPorted

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSDPa:UECategory:REPorted



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Hsdpa_.UeCategory_.Reported.Reported
	:members:
	:undoc-members:
	:noindex: