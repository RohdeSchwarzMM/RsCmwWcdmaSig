Mac
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Cpc_.Mac.Mac
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.cpc.mac.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cell_Cpc_Mac_Cycle.rst