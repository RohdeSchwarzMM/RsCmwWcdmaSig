Dcarrier
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:ULLogging:DCARrier
	single: READ:WCDMa:SIGNaling<Instance>:ULLogging:DCARrier

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:ULLogging:DCARrier
	READ:WCDMa:SIGNaling<Instance>:ULLogging:DCARrier



.. autoclass:: RsCmwWcdmaSig.Implementations.UplinkLogging_.Dcarrier.Dcarrier
	:members:
	:undoc-members:
	:noindex: