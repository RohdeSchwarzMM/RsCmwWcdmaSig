Channel
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.RfSettings_.UserDefined_.Channel.Channel
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.userDefined.channel.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_UserDefined_Channel_Downlink.rst
	Configure_RfSettings_UserDefined_Channel_Uplink.rst