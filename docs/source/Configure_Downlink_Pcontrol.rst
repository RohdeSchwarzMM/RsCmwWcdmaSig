Pcontrol
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:PCONtrol:MODE
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:PCONtrol:STEP
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:PCONtrol:DTQuality
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:PCONtrol:FTERate

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:DL:PCONtrol:MODE
	CONFigure:WCDMa:SIGNaling<Instance>:DL:PCONtrol:STEP
	CONFigure:WCDMa:SIGNaling<Instance>:DL:PCONtrol:DTQuality
	CONFigure:WCDMa:SIGNaling<Instance>:DL:PCONtrol:FTERate



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Downlink_.Pcontrol.Pcontrol
	:members:
	:undoc-members:
	:noindex: