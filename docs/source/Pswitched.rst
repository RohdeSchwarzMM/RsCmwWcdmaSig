Pswitched
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Pswitched.Pswitched
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.pswitched.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Pswitched_State.rst