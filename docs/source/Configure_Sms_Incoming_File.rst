File
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:SMS:INComing:FILE:INFO
	single: CONFigure:WCDMa:SIGNaling<Instance>:SMS:INComing:FILE

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:SMS:INComing:FILE:INFO
	CONFigure:WCDMa:SIGNaling<Instance>:SMS:INComing:FILE



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Sms_.Incoming_.File.File
	:members:
	:undoc-members:
	:noindex: