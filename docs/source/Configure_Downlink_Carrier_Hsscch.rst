Hsscch<HSSCch>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: No1 .. No4
	rc = driver.configure.downlink.carrier.hsscch.repcap_hSSCch_get()
	driver.configure.downlink.carrier.hsscch.repcap_hSSCch_set(repcap.HSSCch.No1)





.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Downlink_.Carrier_.Hsscch.Hsscch
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.downlink.carrier.hsscch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Downlink_Carrier_Hsscch_UeId.rst
	Configure_Downlink_Carrier_Hsscch_IdDummy.rst