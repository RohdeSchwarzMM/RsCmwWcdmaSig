Sdu
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Throughput_.Trace_.Uplink_.Sdu.Sdu
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.throughput.trace.uplink.sdu.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Throughput_Trace_Uplink_Sdu_Current.rst
	Throughput_Trace_Uplink_Sdu_Average.rst