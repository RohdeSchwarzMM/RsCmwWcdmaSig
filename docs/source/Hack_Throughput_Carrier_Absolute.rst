Absolute
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:HACK:THRoughput:CARRier<Carrier>:ABSolute
	single: READ:WCDMa:SIGNaling<Instance>:HACK:THRoughput:CARRier<Carrier>:ABSolute

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:HACK:THRoughput:CARRier<Carrier>:ABSolute
	READ:WCDMa:SIGNaling<Instance>:HACK:THRoughput:CARRier<Carrier>:ABSolute



.. autoclass:: RsCmwWcdmaSig.Implementations.Hack_.Throughput_.Carrier_.Absolute.Absolute
	:members:
	:undoc-members:
	:noindex: