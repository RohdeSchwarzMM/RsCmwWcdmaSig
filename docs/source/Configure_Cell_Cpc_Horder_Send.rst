Send
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:HORDer:SEND

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:HORDer:SEND



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Cpc_.Horder_.Send.Send
	:members:
	:undoc-members:
	:noindex: