Mcqi
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Hack_.Mcqi.Mcqi
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.hack.mcqi.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Hack_Mcqi_Carrier.rst