Slot
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:ULLogging:SLOT
	single: READ:WCDMa:SIGNaling<Instance>:ULLogging:SLOT

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:ULLogging:SLOT
	READ:WCDMa:SIGNaling<Instance>:ULLogging:SLOT



.. autoclass:: RsCmwWcdmaSig.Implementations.UplinkLogging_.Slot.Slot
	:members:
	:undoc-members:
	:noindex: