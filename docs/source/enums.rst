Enums
=========

AckNack
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AckNack.ACK
	# All values (3x):
	ACK | DTX | NACK

AmrCodecModeNarrow
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.AmrCodecModeNarrow.A
	# Last value:
	value = enums.AmrCodecModeNarrow.M
	# All values (9x):
	A | B | C | D | E | F | G | H
	M

AmrCodecModeWide
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.AmrCodecModeWide.A
	# Last value:
	value = enums.AmrCodecModeWide.M2
	# All values (12x):
	A | B | C | D | E | F | G | H
	I | M | M1 | M2

AutoManualMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AutoManualMode.AUTO
	# All values (2x):
	AUTO | MANual

AveragingMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AveragingMode.CONTinuous
	# All values (2x):
	CONTinuous | WINDow

BandClass
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.BandClass.AWS
	# Last value:
	value = enums.BandClass.USPC
	# All values (21x):
	AWS | B18M | IEXT | IM2K | JTAC | KCEL | KPCS | LO7C
	N45T | NA7C | NA8S | NA9C | NAPC | PA4M | PA8M | PS7C
	TACS | U25B | U25F | USC | USPC

BasebandBoard
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.BasebandBoard.BBR1
	# Last value:
	value = enums.BasebandBoard.SUW44
	# All values (140x):
	BBR1 | BBR11 | BBR12 | BBR13 | BBR14 | BBR2 | BBR21 | BBR22
	BBR23 | BBR24 | BBR3 | BBR31 | BBR32 | BBR33 | BBR34 | BBR4
	BBR41 | BBR42 | BBR43 | BBR44 | BBT1 | BBT11 | BBT12 | BBT13
	BBT14 | BBT2 | BBT21 | BBT22 | BBT23 | BBT24 | BBT3 | BBT31
	BBT32 | BBT33 | BBT34 | BBT4 | BBT41 | BBT42 | BBT43 | BBT44
	SUA012 | SUA034 | SUA056 | SUA078 | SUA1 | SUA11 | SUA112 | SUA12
	SUA13 | SUA134 | SUA14 | SUA15 | SUA156 | SUA16 | SUA17 | SUA178
	SUA18 | SUA2 | SUA21 | SUA212 | SUA22 | SUA23 | SUA234 | SUA24
	SUA25 | SUA256 | SUA26 | SUA27 | SUA278 | SUA28 | SUA3 | SUA31
	SUA312 | SUA32 | SUA33 | SUA334 | SUA34 | SUA35 | SUA356 | SUA36
	SUA37 | SUA378 | SUA38 | SUA4 | SUA41 | SUA412 | SUA42 | SUA43
	SUA434 | SUA44 | SUA45 | SUA456 | SUA46 | SUA47 | SUA478 | SUA48
	SUA5 | SUA6 | SUA7 | SUA8 | SUU1 | SUU11 | SUU12 | SUU13
	SUU14 | SUU2 | SUU21 | SUU22 | SUU23 | SUU24 | SUU3 | SUU31
	SUU32 | SUU33 | SUU34 | SUU4 | SUU41 | SUU42 | SUU43 | SUU44
	SUW1 | SUW11 | SUW12 | SUW13 | SUW14 | SUW2 | SUW21 | SUW22
	SUW23 | SUW24 | SUW3 | SUW31 | SUW32 | SUW33 | SUW34 | SUW4
	SUW41 | SUW42 | SUW43 | SUW44

BitPattern
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BitPattern.ALL0
	# All values (7x):
	ALL0 | ALL1 | ALTernating | PRBS11 | PRBS13 | PRBS15 | PRBS9

Bsic
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Bsic.NONVerified
	# All values (2x):
	NONVerified | VERified

BtfdDataRate
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.BtfdDataRate.R10K2
	# Last value:
	value = enums.BtfdDataRate.R7K95
	# All values (9x):
	R10K2 | R12K2 | R1K95 | R4K75 | R5K15 | R5K9 | R6K7 | R7K4
	R7K95

CallRelease
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CallRelease.LOCal
	# All values (2x):
	LOCal | NORMal

CbsMessageSeverity
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.CbsMessageSeverity.AAMBer
	# Last value:
	value = enums.CbsMessageSeverity.UDEFined
	# All values (9x):
	AAMBer | AEXTreme | APResidentia | ASEVere | EARThquake | ETWarning | ETWTest | TSUNami
	UDEFined

CellConfig
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.CellConfig._3CHS
	# Last value:
	value = enums.CellConfig.WCDMa
	# All values (16x):
	_3CHS | _3DUPlus | _3HDU | _4CHS | _4DUPlus | _4HDU | DCHS | DDUPlus
	DHDU | HDUPlus | HSDPa | HSPA | HSPLus | HSUPa | QPSK | WCDMa

CellPower
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CellPower.NAV
	# All values (4x):
	NAV | OFL | OK | UFL

ChannelType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ChannelType.CQI
	# All values (3x):
	CQI | FIXed | UDEFined

Cipher
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Cipher.UEA0
	# All values (3x):
	UEA0 | UEA1 | UEA2

ClosedLoopPower
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ClosedLoopPower.DPCH
	# All values (2x):
	DPCH | TOTal

CmodeActivation
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CmodeActivation.MEASurement
	# All values (2x):
	MEASurement | RAB

CmodePatternSelection
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CmodePatternSelection.NONE
	# All values (4x):
	NONE | SINGle | UEReport | ULCM

CmSerRejectType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CmSerRejectType.ECALl
	# All values (7x):
	ECALl | ECSMs | NCALl | NCECall | NCSMs | NESMs | SMS

CodingGroup
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CodingGroup.DCMClass
	# All values (3x):
	DCMClass | GDCoding | REServed

CompressedMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CompressedMode.NN
	# All values (4x):
	NN | NY | YN | YY

CompressedModeBand
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.CompressedModeBand.OB1
	# Last value:
	value = enums.CompressedModeBand.OB9
	# All values (25x):
	OB1 | OB10 | OB11 | OB12 | OB13 | OB14 | OB15 | OB16
	OB17 | OB18 | OB19 | OB2 | OB20 | OB21 | OB22 | OB25
	OB26 | OB3 | OB32 | OB4 | OB5 | OB6 | OB7 | OB8
	OB9

Condition
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Condition.ALTernating
	# All values (5x):
	ALTernating | MAXPower | MINPower | NONE | TPOWer

ConditionB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ConditionB.ALTernating
	# All values (4x):
	ALTernating | MAXPower | MINPower | TPOWer

CounterValue
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CounterValue.N1
	# All values (8x):
	N1 | N10 | N100 | N2 | N20 | N200 | N4 | N50

Cqi
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Cqi._0
	# Last value:
	value = enums.Cqi.DTX
	# All values (32x):
	_0 | _1 | _10 | _11 | _12 | _13 | _14 | _15
	_16 | _17 | _18 | _19 | _2 | _20 | _21 | _22
	_23 | _24 | _25 | _26 | _27 | _28 | _29 | _3
	_30 | _4 | _5 | _6 | _7 | _8 | _9 | DTX

CsFallbackConnectionType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CsFallbackConnectionType.TMRMc
	# All values (2x):
	TMRMc | VOICe

CswitchedAction
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CswitchedAction.CONNect
	# All values (5x):
	CONNect | DISConnect | HANDover | SSMS | UNRegister

CswitchedState
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.CswitchedState.ALERting
	# Last value:
	value = enums.CswitchedState.SIGNaling
	# All values (15x):
	ALERting | CESTablished | CONNecting | IHANdover | IHPReparate | IREDirection | IRPReparate | OFF
	OHANdover | ON | OREDirection | PAGing | REGister | RELeasing | SIGNaling

CurrentConnectionType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CurrentConnectionType.NONE
	# All values (8x):
	NONE | PACKet | SRB | TEST | VIDeo | VIPacket | VOICe | VOPacket

DataRateDownlink
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DataRateDownlink.HSDPa
	# All values (7x):
	HSDPa | R128 | R16 | R32 | R384 | R64 | R8

DataRateUplink
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DataRateUplink.HSUPa
	# All values (7x):
	HSUPa | R128 | R16 | R32 | R384 | R64 | R8

DchEnhanced
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DchEnhanced.BASic
	# All values (3x):
	BASic | FULL | NO

DestinationState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DestinationState.CPCH
	# All values (4x):
	CPCH | FACH | IDLE | UPCH

DsTime
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DsTime.OFF
	# All values (4x):
	OFF | ON | P1H | P2H

EhichIndicatorMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.EhichIndicatorMode.ACK
	# All values (5x):
	ACK | ALTernating | CRC | DTX | NACK

Enable
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Enable.ON
	# All values (1x):
	ON

ErgchIndicatorMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ErgchIndicatorMode.ALTernating
	# All values (7x):
	ALTernating | CONTinuous | DOWN | DTX | HARQ | SINGle | UP

Etfci
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Etfci._0
	# Last value:
	value = enums.Etfci.DTX
	# All values (129x):
	_0 | _1 | _10 | _100 | _101 | _102 | _103 | _104
	_105 | _106 | _107 | _108 | _109 | _11 | _110 | _111
	_112 | _113 | _114 | _115 | _116 | _117 | _118 | _119
	_12 | _120 | _121 | _122 | _123 | _124 | _125 | _126
	_127 | _13 | _14 | _15 | _16 | _17 | _18 | _19
	_2 | _20 | _21 | _22 | _23 | _24 | _25 | _26
	_27 | _28 | _29 | _3 | _30 | _31 | _32 | _33
	_34 | _35 | _36 | _37 | _38 | _39 | _4 | _40
	_41 | _42 | _43 | _44 | _45 | _46 | _47 | _48
	_49 | _5 | _50 | _51 | _52 | _53 | _54 | _55
	_56 | _57 | _58 | _59 | _6 | _60 | _61 | _62
	_63 | _64 | _65 | _66 | _67 | _68 | _69 | _7
	_70 | _71 | _72 | _73 | _74 | _75 | _76 | _77
	_78 | _79 | _8 | _80 | _81 | _82 | _83 | _84
	_85 | _86 | _87 | _88 | _89 | _9 | _90 | _91
	_92 | _93 | _94 | _95 | _96 | _97 | _98 | _99
	DTX

Fader
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Fader.FAD012
	# Last value:
	value = enums.Fader.FAD8
	# All values (60x):
	FAD012 | FAD034 | FAD056 | FAD078 | FAD1 | FAD11 | FAD112 | FAD12
	FAD13 | FAD134 | FAD14 | FAD15 | FAD156 | FAD16 | FAD17 | FAD178
	FAD18 | FAD2 | FAD21 | FAD212 | FAD22 | FAD23 | FAD234 | FAD24
	FAD25 | FAD256 | FAD26 | FAD27 | FAD278 | FAD28 | FAD3 | FAD31
	FAD312 | FAD32 | FAD33 | FAD334 | FAD34 | FAD35 | FAD356 | FAD36
	FAD37 | FAD378 | FAD38 | FAD4 | FAD41 | FAD412 | FAD42 | FAD43
	FAD434 | FAD44 | FAD45 | FAD456 | FAD46 | FAD47 | FAD478 | FAD48
	FAD5 | FAD6 | FAD7 | FAD8

FadingStandard
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.FadingStandard.B261
	# Last value:
	value = enums.FadingStandard.VA30
	# All values (19x):
	B261 | B262 | B263 | BDEath | C1 | C2 | C3 | C4
	C5 | C6 | C8 | HST | MPRopagation | PA3 | PB3 | USER
	VA12 | VA3 | VA30

FilledBlocks
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.FilledBlocks.P0031
	# Last value:
	value = enums.FilledBlocks.P1000
	# All values (17x):
	P0031 | P0033 | P0036 | P0038 | P0042 | P0045 | P0050 | P0056
	P0062 | P0071 | P0083 | P0100 | P0125 | P0167 | P0250 | P0500
	P1000

GapSize
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.GapSize.ANY
	# All values (3x):
	ANY | M10 | M5

GeneratorState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.GeneratorState.OFF
	# All values (3x):
	OFF | ON | RFHandover

GeoScope
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.GeoScope.CIMMediate
	# All values (4x):
	CIMMediate | CNORmal | PLMN | SERVice

GsmBand
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.GsmBand.G04
	# All values (5x):
	G04 | G085 | G09 | G18 | G19

Handover
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Handover.PACKet
	# All values (3x):
	PACKet | TM | VOICe

HappyBit
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HappyBit.DTX
	# All values (3x):
	DTX | HAPPy | UNHappy

HoverExtDestination
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HoverExtDestination.CDMA
	# All values (5x):
	CDMA | EVDO | GSM | LTE | WCDMa

HrVersion
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HrVersion.RV0
	# All values (2x):
	RV0 | TABLe

HsdpaModulation
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HsdpaModulation.Q16
	# All values (3x):
	Q16 | Q64 | QPSK

HsetFixed
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.HsetFixed.H1AI
	# Last value:
	value = enums.HsetFixed.HCMT
	# All values (45x):
	H1AI | H1BI | H1CI | H1M1 | H1M2 | H1MI | H2M1 | H2M2
	H3A1 | H3A2 | H3B1 | H3B2 | H3C1 | H3C2 | H3M1 | H3M2
	H4M1 | H5M1 | H6A1 | H6A2 | H6B1 | H6B2 | H6C1 | H6C2
	H6M1 | H6M2 | H8A3 | H8AI | H8B3 | H8BI | H8C3 | H8CI
	H8M3 | H8MI | H8MT | HAA1 | HAA2 | HAB1 | HAB2 | HAC1
	HAC2 | HAM1 | HAM2 | HCM1 | HCMT

HspaTestModeDirection
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HspaTestModeDirection.HSDPa
	# All values (2x):
	HSDPa | HSPA

HsScchType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HsScchType.AUTomatic
	# All values (6x):
	AUTomatic | CH1 | CH2 | CH3 | CH4 | RANDom

HsupaModulation
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HsupaModulation.Q16
	# All values (2x):
	Q16 | QPSK

InsertLossMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.InsertLossMode.NORMal
	# All values (2x):
	NORMal | USER

IpAddrIndex
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IpAddrIndex.IP1
	# All values (3x):
	IP1 | IP2 | IP3

LevelSeqState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LevelSeqState.FAILed
	# All values (5x):
	FAILed | IDLE | RUNNing | SCHanged | SCONflict

LogCategory
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LogCategory.CONTinue
	# All values (4x):
	CONTinue | ERRor | INFO | WARNing

LongSmsHandling
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LongSmsHandling.MSMS
	# All values (2x):
	MSMS | TRUNcate

LteBand
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.LteBand.OB1
	# Last value:
	value = enums.LteBand.UDEFined
	# All values (68x):
	OB1 | OB10 | OB11 | OB12 | OB13 | OB14 | OB15 | OB16
	OB17 | OB18 | OB19 | OB2 | OB20 | OB21 | OB22 | OB23
	OB24 | OB25 | OB250 | OB252 | OB255 | OB26 | OB27 | OB28
	OB29 | OB3 | OB30 | OB31 | OB32 | OB33 | OB34 | OB35
	OB36 | OB37 | OB38 | OB39 | OB4 | OB40 | OB41 | OB42
	OB43 | OB44 | OB45 | OB46 | OB48 | OB49 | OB5 | OB50
	OB51 | OB52 | OB6 | OB65 | OB66 | OB67 | OB68 | OB69
	OB7 | OB70 | OB71 | OB72 | OB73 | OB74 | OB75 | OB76
	OB8 | OB85 | OB9 | UDEFined

MaxChanCode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MaxChanCode.S16
	# All values (8x):
	S16 | S22 | S224 | S24 | S32 | S4 | S64 | S8

MaxRelVersion
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.MaxRelVersion.AUTO
	# Last value:
	value = enums.MaxRelVersion.R99
	# All values (9x):
	AUTO | R10 | R11 | R5 | R6 | R7 | R8 | R9
	R99

MeasType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasType.GENeral
	# All values (2x):
	GENeral | MISSed

MessageClass
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MessageClass.CL0
	# All values (5x):
	CL0 | CL1 | CL2 | CL3 | NONE

MessageHandling
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MessageHandling.FILE
	# All values (2x):
	FILE | INTernal

MobilityMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MobilityMode.CCORder
	# All values (4x):
	CCORder | HANDover | NAV | REDirection

MonitoredHarq
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.MonitoredHarq.ALL
	# Last value:
	value = enums.MonitoredHarq.H7
	# All values (9x):
	ALL | H0 | H1 | H2 | H3 | H4 | H5 | H6
	H7

NetworkAndGps
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NetworkAndGps.BOTH
	# All values (4x):
	BOTH | NETWork | NONE | UE

NominalPowerMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NominalPowerMode.AUToranging
	# All values (3x):
	AUToranging | MANual | ULPC

NrOfDigits
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NrOfDigits.D2
	# All values (2x):
	D2 | D3

NtOperMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NtOperMode.M1
	# All values (2x):
	M1 | M2

OcnsChannelType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.OcnsChannelType.AUTO
	# All values (5x):
	AUTO | R5 | R6 | R7 | R99

OperationBand
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.OperationBand.OB1
	# Last value:
	value = enums.OperationBand.UDEFined
	# All values (30x):
	OB1 | OB10 | OB11 | OB12 | OB13 | OB14 | OB15 | OB16
	OB17 | OB18 | OB19 | OB2 | OB20 | OB21 | OB22 | OB25
	OB26 | OB3 | OB32 | OB4 | OB5 | OB6 | OB7 | OB8
	OB9 | OBL1 | OBS1 | OBS2 | OBS3 | UDEFined

OperBandConfig
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.OperBandConfig.C1
	# All values (7x):
	C1 | C2 | C3 | C4 | C5 | C6 | UDEFined

ParameterType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ParameterType.PRIMary
	# All values (2x):
	PRIMary | SECondary

PatternType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PatternType.DU
	# All values (2x):
	DU | UD

PhaseReference
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PhaseReference.PCPich
	# All values (2x):
	PCPich | SCPich

PowerControlMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowerControlMode.M0
	# All values (4x):
	M0 | M1 | OFF | ON

PowerStrategy
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowerStrategy.AF
	# All values (3x):
	AF | BF | CE

Priority
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Priority.BACKground
	# All values (3x):
	BACKground | HIGH | NORMal

Procedure
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Procedure.CSOPs
	# All values (3x):
	CSOPs | CSPS | PS

PswitchedAction
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PswitchedAction.ACONnect
	# All values (4x):
	ACONnect | CONNect | DISConnect | HANDover

PswitchedState
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.PswitchedState.ATTached
	# Last value:
	value = enums.PswitchedState.SIGNaling
	# All values (14x):
	ATTached | CESTablished | CONNecting | IHANdover | IHPReparate | IREDirection | IRPReparate | OFF
	OHANdover | ON | OREDirection | PAGing | RELeasing | SIGNaling

ReducedSignState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ReducedSignState.OFF
	# All values (3x):
	OFF | ON | PROCessing

RefChannelDataRate
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RefChannelDataRate.BTFD
	# All values (6x):
	BTFD | R12K2 | R144k | R384k | R64K | R768k

RejectCause
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RejectCause.CSCongestion
	# All values (6x):
	CSCongestion | CSUNspecific | OFF | ON | PSCongestion | PSUNspecific

RejectionCauseA
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RejectionCauseA.C100
	# Last value:
	value = enums.RejectionCauseA.ON
	# All values (30x):
	C100 | C101 | C11 | C111 | C12 | C13 | C15 | C17
	C2 | C20 | C21 | C22 | C23 | C25 | C3 | C32
	C33 | C34 | C38 | C4 | C48 | C5 | C6 | C95
	C96 | C97 | C98 | C99 | OFF | ON

RejectionCauseB
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RejectionCauseB.C10
	# Last value:
	value = enums.RejectionCauseB.ON
	# All values (38x):
	C10 | C100 | C101 | C11 | C111 | C12 | C13 | C14
	C15 | C16 | C17 | C2 | C20 | C21 | C22 | C23
	C25 | C28 | C3 | C32 | C33 | C34 | C38 | C4
	C40 | C48 | C5 | C6 | C7 | C8 | C9 | C95
	C96 | C97 | C98 | C99 | OFF | ON

Repeat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Repeat.CONTinuous
	# All values (2x):
	CONTinuous | SINGleshot

RepetitionB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RepetitionB.CONTinuous
	# All values (3x):
	CONTinuous | ONCE | SGINit

ResourceState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ResourceState.ACTive
	# All values (8x):
	ACTive | ADJusted | INValid | OFF | PENDing | QUEued | RDY | RUN

ResultState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ResultState.FAIL
	# All values (3x):
	FAIL | PASS | RUN

ResultStatus2
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ResultStatus2.DC
	# Last value:
	value = enums.ResultStatus2.ULEU
	# All values (10x):
	DC | INV | NAV | NCAP | OFF | OFL | OK | UFL
	ULEL | ULEU

RetransmisionSeqNr
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RetransmisionSeqNr._0
	# All values (5x):
	_0 | _1 | _2 | _3 | DTX

RlcMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RlcMode.ACKNowledge
	# All values (2x):
	ACKNowledge | TRANsparent

RmcDomain
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RmcDomain.CS
	# All values (2x):
	CS | PS

RrcState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RrcState.CPCH
	# All values (5x):
	CPCH | DCH | FACH | IDLE | UPCH

RvcSequence
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RvcSequence.S1
	# All values (8x):
	S1 | S2 | S3 | S4 | S5 | S6 | S7 | UDEFined

RxConnector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RxConnector.I11I
	# Last value:
	value = enums.RxConnector.RH8
	# All values (154x):
	I11I | I13I | I15I | I17I | I21I | I23I | I25I | I27I
	I31I | I33I | I35I | I37I | I41I | I43I | I45I | I47I
	IF1 | IF2 | IF3 | IQ1I | IQ3I | IQ5I | IQ7I | R11
	R11C | R12 | R12C | R12I | R13 | R13C | R14 | R14C
	R14I | R15 | R16 | R17 | R18 | R21 | R21C | R22
	R22C | R22I | R23 | R23C | R24 | R24C | R24I | R25
	R26 | R27 | R28 | R31 | R31C | R32 | R32C | R32I
	R33 | R33C | R34 | R34C | R34I | R35 | R36 | R37
	R38 | R41 | R41C | R42 | R42C | R42I | R43 | R43C
	R44 | R44C | R44I | R45 | R46 | R47 | R48 | RA1
	RA2 | RA3 | RA4 | RA5 | RA6 | RA7 | RA8 | RB1
	RB2 | RB3 | RB4 | RB5 | RB6 | RB7 | RB8 | RC1
	RC2 | RC3 | RC4 | RC5 | RC6 | RC7 | RC8 | RD1
	RD2 | RD3 | RD4 | RD5 | RD6 | RD7 | RD8 | RE1
	RE2 | RE3 | RE4 | RE5 | RE6 | RE7 | RE8 | RF1
	RF1C | RF2 | RF2C | RF2I | RF3 | RF3C | RF4 | RF4C
	RF4I | RF5 | RF5C | RF6 | RF6C | RF7 | RF8 | RFAC
	RFBC | RFBI | RG1 | RG2 | RG3 | RG4 | RG5 | RG6
	RG7 | RG8 | RH1 | RH2 | RH3 | RH4 | RH5 | RH6
	RH7 | RH8

RxConverter
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RxConverter.IRX1
	# Last value:
	value = enums.RxConverter.RX44
	# All values (40x):
	IRX1 | IRX11 | IRX12 | IRX13 | IRX14 | IRX2 | IRX21 | IRX22
	IRX23 | IRX24 | IRX3 | IRX31 | IRX32 | IRX33 | IRX34 | IRX4
	IRX41 | IRX42 | IRX43 | IRX44 | RX1 | RX11 | RX12 | RX13
	RX14 | RX2 | RX21 | RX22 | RX23 | RX24 | RX3 | RX31
	RX32 | RX33 | RX34 | RX4 | RX41 | RX42 | RX43 | RX44

SampleRate
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SampleRate.M1
	# All values (8x):
	M1 | M100 | M15 | M19 | M3 | M30 | M7 | M9

Scenario
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Scenario.DBFading
	# Last value:
	value = enums.Scenario.UNDefined
	# All values (12x):
	DBFading | DBFDiversity | DCARrier | DCFading | DCFDiversity | DCHSpa | FCHSpa | SCELl
	SCFading | SCFDiversity | TCHSpa | UNDefined

SimCardType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SimCardType.C2G
	# All values (3x):
	C2G | C3G | MILenage

SlopeType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SlopeType.NEGative
	# All values (2x):
	NEGative | POSitive

SmsDataCoding
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SmsDataCoding.BIT7
	# All values (3x):
	BIT7 | BIT8 | REServed

SourceInt
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SourceInt.EXTernal
	# All values (2x):
	EXTernal | INTernal

SourceTime
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SourceTime.CMWTime
	# All values (2x):
	CMWTime | DATE

SrbDataRate
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SrbDataRate.R13K6
	# All values (4x):
	R13K6 | R1K7 | R2K5 | R3K4

SrbSingleType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SrbSingleType.CDCH
	# All values (2x):
	CDCH | CFACh

StopCondition
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.StopCondition.NONE
	# All values (2x):
	NONE | SLFail

SubTest
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SubTest.S1
	# All values (5x):
	S1 | S2 | S3 | S4 | S5

SucessState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SucessState.FAILed
	# All values (2x):
	FAILed | SUCCessful

Sync
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Sync.NAV
	# All values (3x):
	NAV | NOSYnc | OK

SyncState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SyncState.ADINtermed
	# All values (7x):
	ADINtermed | ADJusted | INValid | OFF | ON | PENDing | RFHandover

TableIndex
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TableIndex.CONFormance
	# All values (4x):
	CONFormance | FIXed | FOLLow | SEQuence

TerminatingType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TerminatingType.RMC
	# All values (5x):
	RMC | SRB | TEST | VIDeo | VOICe

TestCase
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TestCase.AWGN
	# All values (2x):
	AWGN | FADing

TestMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TestMode.HOLD
	# All values (2x):
	HOLD | UPDown

TestModeType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TestModeType.BTFD
	# All values (5x):
	BTFD | FACH | HSPA | RHSPa | RMC

TpcMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TpcMode.A1S1
	# All values (3x):
	A1S1 | A1S2 | A2S1

TpcSetType
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TpcSetType.ALL0
	# Last value:
	value = enums.TpcSetType.ULCM
	# All values (19x):
	ALL0 | ALL1 | ALTernating | CLOop | CONTinuous | CTFC | DHIB | MPEDch
	PHDown | PHUP | SAL0 | SAL1 | SALT | TSABc | TSE | TSEF
	TSF | TSGH | ULCM

TpcState
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TpcState.ALTernating
	# Last value:
	value = enums.TpcState.TRANsition
	# All values (14x):
	ALTernating | CONTinous | FAILed | IDLE | MAXPower | MINPower | MRESource | SCHanged
	SCONflict | SEARching | SINGle | TPLocked | TPUNlocked | TRANsition

TransGapType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TransGapType.AF
	# All values (3x):
	AF | AR | B

TransGapTypeExtended
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TransGapTypeExtended.A
	# All values (8x):
	A | B | C | D | E | F | RFA | RFB

TransTimeInterval
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TransTimeInterval.M10
	# All values (2x):
	M10 | M2

TriggerMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TriggerMode.ONCE
	# All values (2x):
	ONCE | PERiodic

TtiExtended
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TtiExtended.M10
	# All values (4x):
	M10 | M20 | M40 | M80

TxConnector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TxConnector.I12O
	# Last value:
	value = enums.TxConnector.RH18
	# All values (77x):
	I12O | I14O | I16O | I18O | I22O | I24O | I26O | I28O
	I32O | I34O | I36O | I38O | I42O | I44O | I46O | I48O
	IF1 | IF2 | IF3 | IQ2O | IQ4O | IQ6O | IQ8O | R118
	R1183 | R1184 | R11C | R11O | R11O3 | R11O4 | R12C | R13C
	R13O | R14C | R214 | R218 | R21C | R21O | R22C | R23C
	R23O | R24C | R258 | R318 | R31C | R31O | R32C | R33C
	R33O | R34C | R418 | R41C | R41O | R42C | R43C | R43O
	R44C | RA18 | RB14 | RB18 | RC18 | RD18 | RE18 | RF18
	RF1C | RF1O | RF2C | RF3C | RF3O | RF4C | RF5C | RF6C
	RFAC | RFAO | RFBC | RG18 | RH18

TxConverter
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TxConverter.ITX1
	# Last value:
	value = enums.TxConverter.TX44
	# All values (40x):
	ITX1 | ITX11 | ITX12 | ITX13 | ITX14 | ITX2 | ITX21 | ITX22
	ITX23 | ITX24 | ITX3 | ITX31 | ITX32 | ITX33 | ITX34 | ITX4
	ITX41 | ITX42 | ITX43 | ITX44 | TX1 | TX11 | TX12 | TX13
	TX14 | TX2 | TX21 | TX22 | TX23 | TX24 | TX3 | TX31
	TX32 | TX33 | TX34 | TX4 | TX41 | TX42 | TX43 | TX44

UeAlgorithm
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UeAlgorithm.EXTRapolation
	# All values (2x):
	EXTRapolation | INTerpolation

UeNaviSupport
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UeNaviSupport.NETWork
	# All values (4x):
	NETWork | NONE | NUE | UE

UePowerClass
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UePowerClass.PC1
	# All values (5x):
	PC1 | PC2 | PC3 | PC3B | PC4

UnscheduledTransType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UnscheduledTransType.DTX
	# All values (2x):
	DTX | DUMMy

UsedSendMethod
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UsedSendMethod.WDEFault
	# All values (1x):
	WDEFault

UtraMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UtraMode.BOTH
	# All values (3x):
	BOTH | FDD | TDD

UtranTestMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UtranTestMode.MODE1
	# All values (3x):
	MODE1 | MODE2 | OFF

VideoRate
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.VideoRate.R64K
	# All values (1x):
	R64K

VoiceCodec
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.VoiceCodec.NB
	# All values (2x):
	NB | WB

VoiceSource
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.VoiceSource.LOOPback
	# All values (2x):
	LOOPback | SPEech

WizzardSelection
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.WizzardSelection.DHIP
	# All values (8x):
	DHIP | ERGM | HCQI | HDMT | HSMT | HUMP | HUMT | OOS

YesNoStatus
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.YesNoStatus.NO
	# All values (2x):
	NO | YES

Zone
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Zone.NONE
	# All values (2x):
	NONE | Z1

