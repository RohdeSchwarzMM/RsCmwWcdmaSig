MsFrames
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:HCQI:CARRier<Carrier>:MSFRames
	single: READ:WCDMa:SIGNaling<Instance>:HCQI:CARRier<Carrier>:MSFRames

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:HCQI:CARRier<Carrier>:MSFRames
	READ:WCDMa:SIGNaling<Instance>:HCQI:CARRier<Carrier>:MSFRames



.. autoclass:: RsCmwWcdmaSig.Implementations.Hcqi_.Carrier_.MsFrames.MsFrames
	:members:
	:undoc-members:
	:noindex: