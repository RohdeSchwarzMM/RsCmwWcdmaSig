Carrier
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:CARRier<Carrier>:GMTFactor
	single: CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:CARRier<Carrier>:AWGN
	single: CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:CARRier<Carrier>:COPower
	single: CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:CARRier<Carrier>:TOPower

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:CARRier<Carrier>:GMTFactor
	CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:CARRier<Carrier>:AWGN
	CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:CARRier<Carrier>:COPower
	CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:CARRier<Carrier>:TOPower



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.RfSettings_.Carrier.Carrier
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.carrier.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Carrier_Edc.rst
	Configure_RfSettings_Carrier_Eattenuation.rst
	Configure_RfSettings_Carrier_Channel.rst
	Configure_RfSettings_Carrier_Frequency.rst
	Configure_RfSettings_Carrier_Foffset.rst
	Configure_RfSettings_Carrier_Uplink.rst
	Configure_RfSettings_Carrier_Downlink.rst