Dchspa
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:WCDMa:SIGNaling<Instance>:SCENario:DCHSpa:FLEXible
	single: ROUTe:WCDMa:SIGNaling<Instance>:SCENario:DCHSpa

.. code-block:: python

	ROUTe:WCDMa:SIGNaling<Instance>:SCENario:DCHSpa:FLEXible
	ROUTe:WCDMa:SIGNaling<Instance>:SCENario:DCHSpa



.. autoclass:: RsCmwWcdmaSig.Implementations.Route_.Scenario_.Dchspa.Dchspa
	:members:
	:undoc-members:
	:noindex: