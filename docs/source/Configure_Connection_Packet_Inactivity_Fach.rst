Fach
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:PACKet:INACtivity:FACH:TIMer
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:PACKet:INACtivity:FACH:DSTate

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:PACKet:INACtivity:FACH:TIMer
	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:PACKet:INACtivity:FACH:DSTate



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Connection_.Packet_.Inactivity_.Fach.Fach
	:members:
	:undoc-members:
	:noindex: