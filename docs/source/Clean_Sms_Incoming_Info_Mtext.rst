Mtext
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CLEan:WCDMa:SIGNaling<Instance>:SMS:INComing:INFO:MTEXt

.. code-block:: python

	CLEan:WCDMa:SIGNaling<Instance>:SMS:INComing:INFO:MTEXt



.. autoclass:: RsCmwWcdmaSig.Implementations.Clean_.Sms_.Incoming_.Info_.Mtext.Mtext
	:members:
	:undoc-members:
	:noindex: