Eagch
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:EAGCh:TOUT
	single: CONFigure:WCDMa:SIGNaling<Instance>:EAGCh:REPetition
	single: CONFigure:WCDMa:SIGNaling<Instance>:EAGCh:MFRames
	single: CONFigure:WCDMa:SIGNaling<Instance>:EAGCh:MTYPe
	single: CONFigure:WCDMa:SIGNaling<Instance>:EAGCh:LIMit

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:EAGCh:TOUT
	CONFigure:WCDMa:SIGNaling<Instance>:EAGCh:REPetition
	CONFigure:WCDMa:SIGNaling<Instance>:EAGCh:MFRames
	CONFigure:WCDMa:SIGNaling<Instance>:EAGCh:MTYPe
	CONFigure:WCDMa:SIGNaling<Instance>:EAGCh:LIMit



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Eagch.Eagch
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.eagch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Eagch_Etfci.rst