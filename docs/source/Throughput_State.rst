State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:THRoughput:STATe

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:THRoughput:STATe



.. autoclass:: RsCmwWcdmaSig.Implementations.Throughput_.State.State
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.throughput.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Throughput_State_All.rst