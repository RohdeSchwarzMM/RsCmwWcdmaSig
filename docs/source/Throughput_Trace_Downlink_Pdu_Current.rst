Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:THRoughput:TRACe:DL:PDU:CURRent
	single: READ:WCDMa:SIGNaling<Instance>:THRoughput:TRACe:DL:PDU:CURRent

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:THRoughput:TRACe:DL:PDU:CURRent
	READ:WCDMa:SIGNaling<Instance>:THRoughput:TRACe:DL:PDU:CURRent



.. autoclass:: RsCmwWcdmaSig.Implementations.Throughput_.Trace_.Downlink_.Pdu_.Current.Current
	:members:
	:undoc-members:
	:noindex: