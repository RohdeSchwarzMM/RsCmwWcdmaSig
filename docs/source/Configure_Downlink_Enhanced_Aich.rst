Aich
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:ENHanced:AICH:ACKNowledge
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:ENHanced:AICH:TTIMing

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:DL:ENHanced:AICH:ACKNowledge
	CONFigure:WCDMa:SIGNaling<Instance>:DL:ENHanced:AICH:TTIMing



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Downlink_.Enhanced_.Aich.Aich
	:members:
	:undoc-members:
	:noindex: