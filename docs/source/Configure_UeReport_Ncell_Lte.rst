Lte
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:UEReport:NCELl:LTE:ENABle

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:UEReport:NCELl:LTE:ENABle



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.UeReport_.Ncell_.Lte.Lte
	:members:
	:undoc-members:
	:noindex: