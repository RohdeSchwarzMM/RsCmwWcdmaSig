HlOperation
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:HLOPeration:ENABle
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:HLOPeration:NTBLock

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:HLOPeration:ENABle
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:HLOPeration:NTBLock



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Cpc_.HlOperation.HlOperation
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.cpc.hlOperation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cell_Cpc_HlOperation_Tblock.rst
	Configure_Cell_Cpc_HlOperation_ScSupport.rst