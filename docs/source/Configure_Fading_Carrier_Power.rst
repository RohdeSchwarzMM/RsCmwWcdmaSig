Power
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:FADing:CARRier<Carrier>:POWer:SUM

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:FADing:CARRier<Carrier>:POWer:SUM



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Fading_.Carrier_.Power.Power
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.fading.carrier.power.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Fading_Carrier_Power_Noise.rst