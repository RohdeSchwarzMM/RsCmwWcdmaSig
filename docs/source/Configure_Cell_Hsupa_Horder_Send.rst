Send
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSUPa:HORDer:SEND

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSUPa:HORDer:SEND



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Hsupa_.Horder_.Send.Send
	:members:
	:undoc-members:
	:noindex: