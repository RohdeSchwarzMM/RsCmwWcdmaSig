Ocns
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:OCNS:TYPE
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:OCNS:LEVel

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:OCNS:TYPE
	CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:OCNS:LEVel



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Downlink_.Carrier_.Ocns.Ocns
	:members:
	:undoc-members:
	:noindex: