Cqi
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSDPa:CQI:RFACtor
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSDPa:CQI:FBCYcle
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSDPa:CQI:TTI
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSDPa:CQI:HARQ
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSDPa:CQI:TINDex
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSDPa:CQI:SEQuence
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSDPa:CQI:FOLLow

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSDPa:CQI:RFACtor
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSDPa:CQI:FBCYcle
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSDPa:CQI:TTI
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSDPa:CQI:HARQ
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSDPa:CQI:TINDex
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSDPa:CQI:SEQuence
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSDPa:CQI:FOLLow



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Hsdpa_.Cqi.Cqi
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.hsdpa.cqi.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cell_Hsdpa_Cqi_Conformance.rst
	Configure_Cell_Hsdpa_Cqi_RvcSequences.rst