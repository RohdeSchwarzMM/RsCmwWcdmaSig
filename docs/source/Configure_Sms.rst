Sms
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:SMS:KTLoop

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:SMS:KTLoop



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Sms.Sms
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.sms.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Sms_Outgoing.rst
	Configure_Sms_Incoming.rst