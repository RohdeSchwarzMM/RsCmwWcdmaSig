Wcdma
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:UEReport:NCELl:WCDMa:ENABle

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:UEReport:NCELl:WCDMa:ENABle



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.UeReport_.Ncell_.Wcdma.Wcdma
	:members:
	:undoc-members:
	:noindex: