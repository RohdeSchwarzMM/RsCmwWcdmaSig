Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:HACK:TRACe:THRoughput:TOTal:CURRent
	single: READ:WCDMa:SIGNaling<Instance>:HACK:TRACe:THRoughput:TOTal:CURRent

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:HACK:TRACe:THRoughput:TOTal:CURRent
	READ:WCDMa:SIGNaling<Instance>:HACK:TRACe:THRoughput:TOTal:CURRent



.. autoclass:: RsCmwWcdmaSig.Implementations.Hack_.Trace_.Throughput_.Total_.Current.Current
	:members:
	:undoc-members:
	:noindex: