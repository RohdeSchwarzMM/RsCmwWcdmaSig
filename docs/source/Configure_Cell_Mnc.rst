Mnc
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:MNC:DIGits
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:MNC

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:MNC:DIGits
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:MNC



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Mnc.Mnc
	:members:
	:undoc-members:
	:noindex: