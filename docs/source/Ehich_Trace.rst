Trace
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Ehich_.Trace.Trace
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ehich.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Ehich_Trace_MpThroughput.rst
	Ehich_Trace_MeThroughput.rst
	Ehich_Trace_Throughput.rst