Carrier
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.UplinkLogging_.Carrier.Carrier
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uplinkLogging.carrier.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UplinkLogging_Carrier_Etfci.rst
	UplinkLogging_Carrier_Rsn.rst
	UplinkLogging_Carrier_Hbit.rst
	UplinkLogging_Carrier_Dpcch.rst
	UplinkLogging_Carrier_Anack.rst
	UplinkLogging_Carrier_Cqi.rst