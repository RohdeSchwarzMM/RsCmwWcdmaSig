Hsscch<HSSCch>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: No1 .. No4
	rc = driver.configure.downlink.carrier.level.hsscch.repcap_hSSCch_get()
	driver.configure.downlink.carrier.level.hsscch.repcap_hSSCch_set(repcap.HSSCch.No1)



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:LEVel:HSSCch<HSSCch>

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:LEVel:HSSCch<HSSCch>



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Downlink_.Carrier_.Level_.Hsscch.Hsscch
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.downlink.carrier.level.hsscch.clone()