Reported
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSUPa:UECategory:REPorted

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSUPa:UECategory:REPorted



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Hsupa_.UeCategory_.Reported.Reported
	:members:
	:undoc-members:
	:noindex: