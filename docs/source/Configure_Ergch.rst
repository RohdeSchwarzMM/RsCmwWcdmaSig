Ergch
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:ERGCh:TOUT
	single: CONFigure:WCDMa:SIGNaling<Instance>:ERGCh:REPetition
	single: CONFigure:WCDMa:SIGNaling<Instance>:ERGCh:MFRames
	single: CONFigure:WCDMa:SIGNaling<Instance>:ERGCh:LIMit

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:ERGCh:TOUT
	CONFigure:WCDMa:SIGNaling<Instance>:ERGCh:REPetition
	CONFigure:WCDMa:SIGNaling<Instance>:ERGCh:MFRames
	CONFigure:WCDMa:SIGNaling<Instance>:ERGCh:LIMit



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Ergch.Ergch
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.ergch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Ergch_Etfci.rst