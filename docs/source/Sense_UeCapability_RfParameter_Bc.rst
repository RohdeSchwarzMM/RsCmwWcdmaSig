Bc<BandCombination>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr32
	rc = driver.sense.ueCapability.rfParameter.bc.repcap_bandCombination_get()
	driver.sense.ueCapability.rfParameter.bc.repcap_bandCombination_set(repcap.BandCombination.Nr1)



.. rubric:: SCPI Commands

.. index::
	single: SENSe:WCDMa:SIGNaling<Instance>:UECapability:RFParameter:BC<BandCombination>

.. code-block:: python

	SENSe:WCDMa:SIGNaling<Instance>:UECapability:RFParameter:BC<BandCombination>



.. autoclass:: RsCmwWcdmaSig.Implementations.Sense_.UeCapability_.RfParameter_.Bc.Bc
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ueCapability.rfParameter.bc.clone()