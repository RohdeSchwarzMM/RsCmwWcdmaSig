Horder
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSUPa:HORDer:SDCorder
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSUPa:HORDer:SUForder

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSUPa:HORDer:SDCorder
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSUPa:HORDer:SUForder



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Hsupa_.Horder.Horder
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.hsupa.horder.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cell_Hsupa_Horder_Send.rst