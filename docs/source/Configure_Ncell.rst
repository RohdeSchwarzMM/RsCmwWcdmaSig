Ncell<Cell>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr16
	rc = driver.configure.ncell.repcap_cell_get()
	driver.configure.ncell.repcap_cell_set(repcap.Cell.Nr1)





.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Ncell.Ncell
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.ncell.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Ncell_Lte.rst
	Configure_Ncell_Gsm.rst
	Configure_Ncell_Wcdma.rst