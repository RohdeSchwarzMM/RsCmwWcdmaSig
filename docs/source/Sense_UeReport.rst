UeReport
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:WCDMa:SIGNaling<Instance>:UEReport:CCELl

.. code-block:: python

	SENSe:WCDMa:SIGNaling<Instance>:UEReport:CCELl



.. autoclass:: RsCmwWcdmaSig.Implementations.Sense_.UeReport.UeReport
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ueReport.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UeReport_Ncell.rst