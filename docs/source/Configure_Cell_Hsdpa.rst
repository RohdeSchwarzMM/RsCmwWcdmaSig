Hsdpa
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSDPa:ANRFactor
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSDPa:TYPE

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSDPa:ANRFactor
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSDPa:TYPE



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Hsdpa.Hsdpa
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.hsdpa.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cell_Hsdpa_UeCategory.rst
	Configure_Cell_Hsdpa_Fixed.rst
	Configure_Cell_Hsdpa_Cqi.rst
	Configure_Cell_Hsdpa_UserDefined.rst