Scell
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:ULLogging:SCELl
	single: READ:WCDMa:SIGNaling<Instance>:ULLogging:SCELl

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:ULLogging:SCELl
	READ:WCDMa:SIGNaling<Instance>:ULLogging:SCELl



.. autoclass:: RsCmwWcdmaSig.Implementations.UplinkLogging_.Scell.Scell
	:members:
	:undoc-members:
	:noindex: