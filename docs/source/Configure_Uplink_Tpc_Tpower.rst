Tpower
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:TPC:TPOWer:OFFSet
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:TPC:TPOWer:REFerence

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:UL:TPC:TPOWer:OFFSet
	CONFigure:WCDMa:SIGNaling<Instance>:UL:TPC:TPOWer:REFerence



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Uplink_.Tpc_.Tpower.Tpower
	:members:
	:undoc-members:
	:noindex: