Qam<QuadratureAM>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: QAM16 .. QAM64
	rc = driver.configure.cell.hsdpa.cqi.rvcSequences.qam.repcap_quadratureAM_get()
	driver.configure.cell.hsdpa.cqi.rvcSequences.qam.repcap_quadratureAM_set(repcap.QuadratureAM.QAM16)



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSDPa:CQI:RVCSequences:QAM<QuadratureAM>

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSDPa:CQI:RVCSequences:QAM<QuadratureAM>



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Hsdpa_.Cqi_.RvcSequences_.Qam.Qam
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.hsdpa.cqi.rvcSequences.qam.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cell_Hsdpa_Cqi_RvcSequences_Qam_UserDefined.rst