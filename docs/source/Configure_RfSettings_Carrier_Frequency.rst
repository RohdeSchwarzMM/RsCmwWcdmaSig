Frequency
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:CARRier<Carrier>:FREQuency:UL
	single: CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:CARRier<Carrier>:FREQuency:DL

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:CARRier<Carrier>:FREQuency:UL
	CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:CARRier<Carrier>:FREQuency:DL



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.RfSettings_.Carrier_.Frequency.Frequency
	:members:
	:undoc-members:
	:noindex: