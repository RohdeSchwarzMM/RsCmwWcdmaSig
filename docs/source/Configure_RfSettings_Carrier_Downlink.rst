Downlink
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:CARRier<Carrier>:DL

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:CARRier<Carrier>:DL



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.RfSettings_.Carrier_.Downlink.Downlink
	:members:
	:undoc-members:
	:noindex: