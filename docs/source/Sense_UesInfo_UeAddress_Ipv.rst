Ipv<IPversion>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: IPv4 .. IPv6
	rc = driver.sense.uesInfo.ueAddress.ipv.repcap_iPversion_get()
	driver.sense.uesInfo.ueAddress.ipv.repcap_iPversion_set(repcap.IPversion.IPv4)



.. rubric:: SCPI Commands

.. index::
	single: SENSe:WCDMa:SIGNaling<Instance>:UESinfo:UEADdress:IPV<IPversion>

.. code-block:: python

	SENSe:WCDMa:SIGNaling<Instance>:UESinfo:UEADdress:IPV<IPversion>



.. autoclass:: RsCmwWcdmaSig.Implementations.Sense_.UesInfo_.UeAddress_.Ipv.Ipv
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.uesInfo.ueAddress.ipv.clone()