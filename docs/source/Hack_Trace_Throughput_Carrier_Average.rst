Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:HACK:TRACe:THRoughput:CARRier<Carrier>:AVERage
	single: READ:WCDMa:SIGNaling<Instance>:HACK:TRACe:THRoughput:CARRier<Carrier>:AVERage

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:HACK:TRACe:THRoughput:CARRier<Carrier>:AVERage
	READ:WCDMa:SIGNaling<Instance>:HACK:TRACe:THRoughput:CARRier<Carrier>:AVERage



.. autoclass:: RsCmwWcdmaSig.Implementations.Hack_.Trace_.Throughput_.Carrier_.Average.Average
	:members:
	:undoc-members:
	:noindex: