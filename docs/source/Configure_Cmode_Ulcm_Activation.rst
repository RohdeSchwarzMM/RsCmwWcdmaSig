Activation
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CMODe:ULCM:ACTivation

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CMODe:ULCM:ACTivation



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cmode_.Ulcm_.Activation.Activation
	:members:
	:undoc-members:
	:noindex: