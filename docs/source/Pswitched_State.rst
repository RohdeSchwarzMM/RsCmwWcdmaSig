State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:PSWitched:STATe

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:PSWitched:STATe



.. autoclass:: RsCmwWcdmaSig.Implementations.Pswitched_.State.State
	:members:
	:undoc-members:
	:noindex: