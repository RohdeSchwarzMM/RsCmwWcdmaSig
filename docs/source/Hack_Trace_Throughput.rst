Throughput
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Hack_.Trace_.Throughput.Throughput
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.hack.trace.throughput.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Hack_Trace_Throughput_Total.rst
	Hack_Trace_Throughput_Carrier.rst