Tchspa
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:WCDMa:SIGNaling<Instance>:SCENario:TCHSpa:FLEXible
	single: ROUTe:WCDMa:SIGNaling<Instance>:SCENario:TCHSpa

.. code-block:: python

	ROUTe:WCDMa:SIGNaling<Instance>:SCENario:TCHSpa:FLEXible
	ROUTe:WCDMa:SIGNaling<Instance>:SCENario:TCHSpa



.. autoclass:: RsCmwWcdmaSig.Implementations.Route_.Scenario_.Tchspa.Tchspa
	:members:
	:undoc-members:
	:noindex: