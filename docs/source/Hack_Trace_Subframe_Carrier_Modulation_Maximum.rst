Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:SIGNaling<Instance>:HACK:TRACe:SUBFrame:CARRier<Carrier>:MODulation:MAXimum
	single: FETCh:WCDMa:SIGNaling<Instance>:HACK:TRACe:SUBFrame:CARRier<Carrier>:MODulation:MAXimum

.. code-block:: python

	READ:WCDMa:SIGNaling<Instance>:HACK:TRACe:SUBFrame:CARRier<Carrier>:MODulation:MAXimum
	FETCh:WCDMa:SIGNaling<Instance>:HACK:TRACe:SUBFrame:CARRier<Carrier>:MODulation:MAXimum



.. autoclass:: RsCmwWcdmaSig.Implementations.Hack_.Trace_.Subframe_.Carrier_.Modulation_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: