DcFading
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:WCDMa:SIGNaling<Instance>:SCENario:DCFading:EXTernal
	single: ROUTe:WCDMa:SIGNaling<Instance>:SCENario:DCFading:INTernal

.. code-block:: python

	ROUTe:WCDMa:SIGNaling<Instance>:SCENario:DCFading:EXTernal
	ROUTe:WCDMa:SIGNaling<Instance>:SCENario:DCFading:INTernal



.. autoclass:: RsCmwWcdmaSig.Implementations.Route_.Scenario_.DcFading.DcFading
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.scenario.dcFading.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Scenario_DcFading_Flexible.rst