UplinkLogging
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:ULLogging:TOUT
	single: CONFigure:WCDMa:SIGNaling<Instance>:ULLogging:SCCYcle
	single: CONFigure:WCDMa:SIGNaling<Instance>:ULLogging:SSFN
	single: CONFigure:WCDMa:SIGNaling<Instance>:ULLogging:MSFRames
	single: CONFigure:WCDMa:SIGNaling<Instance>:ULLogging:REPetition

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:ULLogging:TOUT
	CONFigure:WCDMa:SIGNaling<Instance>:ULLogging:SCCYcle
	CONFigure:WCDMa:SIGNaling<Instance>:ULLogging:SSFN
	CONFigure:WCDMa:SIGNaling<Instance>:ULLogging:MSFRames
	CONFigure:WCDMa:SIGNaling<Instance>:ULLogging:REPetition



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.UplinkLogging.UplinkLogging
	:members:
	:undoc-members:
	:noindex: