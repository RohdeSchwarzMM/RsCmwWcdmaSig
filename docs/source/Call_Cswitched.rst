Cswitched
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALL:WCDMa:SIGNaling<Instance>:CSWitched:ACTion

.. code-block:: python

	CALL:WCDMa:SIGNaling<Instance>:CSWitched:ACTion



.. autoclass:: RsCmwWcdmaSig.Implementations.Call_.Cswitched.Cswitched
	:members:
	:undoc-members:
	:noindex: