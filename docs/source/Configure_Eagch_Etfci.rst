Etfci
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:EAGCh:ETFCi:MODE
	single: CONFigure:WCDMa:SIGNaling<Instance>:EAGCh:ETFCi:MANual
	single: CONFigure:WCDMa:SIGNaling<Instance>:EAGCh:ETFCi:AUTO

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:EAGCh:ETFCi:MODE
	CONFigure:WCDMa:SIGNaling<Instance>:EAGCh:ETFCi:MANual
	CONFigure:WCDMa:SIGNaling<Instance>:EAGCh:ETFCi:AUTO



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Eagch_.Etfci.Etfci
	:members:
	:undoc-members:
	:noindex: