Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:THRoughput:TRACe:UL:SDU:AVERage
	single: READ:WCDMa:SIGNaling<Instance>:THRoughput:TRACe:UL:SDU:AVERage

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:THRoughput:TRACe:UL:SDU:AVERage
	READ:WCDMa:SIGNaling<Instance>:THRoughput:TRACe:UL:SDU:AVERage



.. autoclass:: RsCmwWcdmaSig.Implementations.Throughput_.Trace_.Uplink_.Sdu_.Average.Average
	:members:
	:undoc-members:
	:noindex: