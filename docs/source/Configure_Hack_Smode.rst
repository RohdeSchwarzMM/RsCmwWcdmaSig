Smode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:HACK:SMODe:AVERage

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:HACK:SMODe:AVERage



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Hack_.Smode.Smode
	:members:
	:undoc-members:
	:noindex: