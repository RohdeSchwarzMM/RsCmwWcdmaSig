Carrier
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Ehich_.Trace_.MpThroughput_.Carrier.Carrier
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ehich.trace.mpThroughput.carrier.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Ehich_Trace_MpThroughput_Carrier_Current.rst