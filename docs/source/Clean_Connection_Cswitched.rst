Cswitched
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Clean_.Connection_.Cswitched.Cswitched
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.clean.connection.cswitched.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Clean_Connection_Cswitched_Attempt.rst
	Clean_Connection_Cswitched_Reject.rst