External
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: PREPare:WCDMa:SIGNaling<Instance>:HANDover:EXTernal:DESTination
	single: PREPare:WCDMa:SIGNaling<Instance>:HANDover:EXTernal:LTE
	single: PREPare:WCDMa:SIGNaling<Instance>:HANDover:EXTernal:GSM
	single: PREPare:WCDMa:SIGNaling<Instance>:HANDover:EXTernal:CDMA
	single: PREPare:WCDMa:SIGNaling<Instance>:HANDover:EXTernal:EVDO
	single: PREPare:WCDMa:SIGNaling<Instance>:HANDover:EXTernal:WCDMa

.. code-block:: python

	PREPare:WCDMa:SIGNaling<Instance>:HANDover:EXTernal:DESTination
	PREPare:WCDMa:SIGNaling<Instance>:HANDover:EXTernal:LTE
	PREPare:WCDMa:SIGNaling<Instance>:HANDover:EXTernal:GSM
	PREPare:WCDMa:SIGNaling<Instance>:HANDover:EXTernal:CDMA
	PREPare:WCDMa:SIGNaling<Instance>:HANDover:EXTernal:EVDO
	PREPare:WCDMa:SIGNaling<Instance>:HANDover:EXTernal:WCDMa



.. autoclass:: RsCmwWcdmaSig.Implementations.Prepare_.Handover_.External.External
	:members:
	:undoc-members:
	:noindex: