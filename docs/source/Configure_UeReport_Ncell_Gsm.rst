Gsm
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:UEReport:NCELl:GSM:ENABle

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:UEReport:NCELl:GSM:ENABle



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.UeReport_.Ncell_.Gsm.Gsm
	:members:
	:undoc-members:
	:noindex: