Tpc
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:CARRier<Carrier>:TPC:TPOWer

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:UL:CARRier<Carrier>:TPC:TPOWer



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Uplink_.Carrier_.Tpc.Tpc
	:members:
	:undoc-members:
	:noindex: