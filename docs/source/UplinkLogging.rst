UplinkLogging
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STOP:WCDMa:SIGNaling<Instance>:ULLogging
	single: ABORt:WCDMa:SIGNaling<Instance>:ULLogging
	single: INITiate:WCDMa:SIGNaling<Instance>:ULLogging

.. code-block:: python

	STOP:WCDMa:SIGNaling<Instance>:ULLogging
	ABORt:WCDMa:SIGNaling<Instance>:ULLogging
	INITiate:WCDMa:SIGNaling<Instance>:ULLogging



.. autoclass:: RsCmwWcdmaSig.Implementations.UplinkLogging.UplinkLogging
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.uplinkLogging.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UplinkLogging_State.rst
	UplinkLogging_Sfn.rst
	UplinkLogging_Slot.rst
	UplinkLogging_Carrier.rst
	UplinkLogging_Scell.rst
	UplinkLogging_Dcarrier.rst
	UplinkLogging_Dchspa.rst