Flexible
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:WCDMa:SIGNaling<Instance>:SCENario:DBFading:FLEXible:EXTernal
	single: ROUTe:WCDMa:SIGNaling<Instance>:SCENario:DBFading:FLEXible:INTernal

.. code-block:: python

	ROUTe:WCDMa:SIGNaling<Instance>:SCENario:DBFading:FLEXible:EXTernal
	ROUTe:WCDMa:SIGNaling<Instance>:SCENario:DBFading:FLEXible:INTernal



.. autoclass:: RsCmwWcdmaSig.Implementations.Route_.Scenario_.DbFading_.Flexible.Flexible
	:members:
	:undoc-members:
	:noindex: