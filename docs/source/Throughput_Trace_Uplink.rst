Uplink
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Throughput_.Trace_.Uplink.Uplink
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.throughput.trace.uplink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Throughput_Trace_Uplink_Sdu.rst
	Throughput_Trace_Uplink_Pdu.rst