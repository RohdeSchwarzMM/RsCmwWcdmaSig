Cswitched
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:CSWitched:CRELease

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:CSWitched:CRELease



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Connection_.Cswitched.Cswitched
	:members:
	:undoc-members:
	:noindex: