Ehich
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STOP:WCDMa:SIGNaling<Instance>:EHICh
	single: ABORt:WCDMa:SIGNaling<Instance>:EHICh
	single: INITiate:WCDMa:SIGNaling<Instance>:EHICh

.. code-block:: python

	STOP:WCDMa:SIGNaling<Instance>:EHICh
	ABORt:WCDMa:SIGNaling<Instance>:EHICh
	INITiate:WCDMa:SIGNaling<Instance>:EHICh



.. autoclass:: RsCmwWcdmaSig.Implementations.Ehich.Ehich
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ehich.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Ehich_State.rst
	Ehich_Trace.rst
	Ehich_Carrier.rst
	Ehich_Throughput.rst