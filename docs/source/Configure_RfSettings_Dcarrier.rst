Dcarrier
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:DCARrier:SEParation

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:DCARrier:SEParation



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.RfSettings_.Dcarrier.Dcarrier
	:members:
	:undoc-members:
	:noindex: