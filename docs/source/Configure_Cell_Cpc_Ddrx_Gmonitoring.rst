Gmonitoring
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:DDRX:GMONitoring:ENABle
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:DDRX:GMONitoring:ITHReshold

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:DDRX:GMONitoring:ENABle
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:DDRX:GMONitoring:ITHReshold



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Cpc_.Ddrx_.Gmonitoring.Gmonitoring
	:members:
	:undoc-members:
	:noindex: