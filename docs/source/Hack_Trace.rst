Trace
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Hack_.Trace.Trace
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.hack.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Hack_Trace_Subframe.rst
	Hack_Trace_Throughput.rst
	Hack_Trace_Mcqi.rst