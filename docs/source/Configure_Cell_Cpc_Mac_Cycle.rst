Cycle
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:MAC:CYCLe:ITHReshold

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:MAC:CYCLe:ITHReshold



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Cpc_.Mac_.Cycle.Cycle
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.cpc.mac.cycle.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cell_Cpc_Mac_Cycle_Tti.rst