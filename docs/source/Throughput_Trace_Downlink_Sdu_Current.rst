Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:THRoughput:TRACe:DL:SDU:CURRent
	single: READ:WCDMa:SIGNaling<Instance>:THRoughput:TRACe:DL:SDU:CURRent

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:THRoughput:TRACe:DL:SDU:CURRent
	READ:WCDMa:SIGNaling<Instance>:THRoughput:TRACe:DL:SDU:CURRent



.. autoclass:: RsCmwWcdmaSig.Implementations.Throughput_.Trace_.Downlink_.Sdu_.Current.Current
	:members:
	:undoc-members:
	:noindex: