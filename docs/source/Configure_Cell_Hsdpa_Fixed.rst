Fixed
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSDPa:FIXed:HSET

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSDPa:FIXed:HSET



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Hsdpa_.Fixed.Fixed
	:members:
	:undoc-members:
	:noindex: