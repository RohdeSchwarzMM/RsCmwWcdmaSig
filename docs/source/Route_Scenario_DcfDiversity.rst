DcfDiversity
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:WCDMa:SIGNaling<Instance>:SCENario:DCFDiversity:EXTernal
	single: ROUTe:WCDMa:SIGNaling<Instance>:SCENario:DCFDiversity:INTernal

.. code-block:: python

	ROUTe:WCDMa:SIGNaling<Instance>:SCENario:DCFDiversity:EXTernal
	ROUTe:WCDMa:SIGNaling<Instance>:SCENario:DCFDiversity:INTernal



.. autoclass:: RsCmwWcdmaSig.Implementations.Route_.Scenario_.DcfDiversity.DcfDiversity
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.scenario.dcfDiversity.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Scenario_DcfDiversity_Flexible.rst