Precondition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:TPCSet:PRECondition:PHDown
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:TPCSet:PRECondition:PHUP
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:TPCSet:PRECondition:CONTinuous
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:TPCSet:PRECondition:SINGle

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:UL:TPCSet:PRECondition:PHDown
	CONFigure:WCDMa:SIGNaling<Instance>:UL:TPCSet:PRECondition:PHUP
	CONFigure:WCDMa:SIGNaling<Instance>:UL:TPCSet:PRECondition:CONTinuous
	CONFigure:WCDMa:SIGNaling<Instance>:UL:TPCSet:PRECondition:SINGle



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Uplink_.Tpcset_.Precondition.Precondition
	:members:
	:undoc-members:
	:noindex: