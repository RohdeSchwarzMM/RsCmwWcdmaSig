Carrier
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:HACK:BLER:CARRier<Carrier>
	single: READ:WCDMa:SIGNaling<Instance>:HACK:BLER:CARRier<Carrier>

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:HACK:BLER:CARRier<Carrier>
	READ:WCDMa:SIGNaling<Instance>:HACK:BLER:CARRier<Carrier>



.. autoclass:: RsCmwWcdmaSig.Implementations.Hack_.Bler_.Carrier.Carrier
	:members:
	:undoc-members:
	:noindex: