Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:THRoughput:TRACe:UL:SDU:CURRent
	single: READ:WCDMa:SIGNaling<Instance>:THRoughput:TRACe:UL:SDU:CURRent

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:THRoughput:TRACe:UL:SDU:CURRent
	READ:WCDMa:SIGNaling<Instance>:THRoughput:TRACe:UL:SDU:CURRent



.. autoclass:: RsCmwWcdmaSig.Implementations.Throughput_.Trace_.Uplink_.Sdu_.Current.Current
	:members:
	:undoc-members:
	:noindex: