Scell
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:WCDMa:SIGNaling<Instance>:SCENario:SCELl:FLEXible
	single: ROUTe:WCDMa:SIGNaling<Instance>:SCENario:SCELl

.. code-block:: python

	ROUTe:WCDMa:SIGNaling<Instance>:SCENario:SCELl:FLEXible
	ROUTe:WCDMa:SIGNaling<Instance>:SCENario:SCELl



.. autoclass:: RsCmwWcdmaSig.Implementations.Route_.Scenario_.Scell.Scell
	:members:
	:undoc-members:
	:noindex: