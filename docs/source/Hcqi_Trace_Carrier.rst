Carrier
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:HCQI:TRACe:CARRier<Carrier>
	single: READ:WCDMa:SIGNaling<Instance>:HCQI:TRACe:CARRier<Carrier>

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:HCQI:TRACe:CARRier<Carrier>
	READ:WCDMa:SIGNaling<Instance>:HCQI:TRACe:CARRier<Carrier>



.. autoclass:: RsCmwWcdmaSig.Implementations.Hcqi_.Trace_.Carrier.Carrier
	:members:
	:undoc-members:
	:noindex: