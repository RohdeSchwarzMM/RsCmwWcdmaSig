Ctch
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CBS:CTCH:ENABle
	single: CONFigure:WCDMa:SIGNaling<Instance>:CBS:CTCH:PERiod
	single: CONFigure:WCDMa:SIGNaling<Instance>:CBS:CTCH:FOFFset
	single: CONFigure:WCDMa:SIGNaling<Instance>:CBS:CTCH:FMPLength

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CBS:CTCH:ENABle
	CONFigure:WCDMa:SIGNaling<Instance>:CBS:CTCH:PERiod
	CONFigure:WCDMa:SIGNaling<Instance>:CBS:CTCH:FOFFset
	CONFigure:WCDMa:SIGNaling<Instance>:CBS:CTCH:FMPLength



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cbs_.Ctch.Ctch
	:members:
	:undoc-members:
	:noindex: