Ddrx
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:DDRX:ENABle

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:DDRX:ENABle



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Cpc_.Ddrx.Ddrx
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.cpc.ddrx.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cell_Cpc_Ddrx_Cycle.rst
	Configure_Cell_Cpc_Ddrx_Gmonitoring.rst