Carrier
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:SIGNaling<Instance>:EHICh:CARRier<Carrier>
	single: FETCh:WCDMa:SIGNaling<Instance>:EHICh:CARRier<Carrier>

.. code-block:: python

	READ:WCDMa:SIGNaling<Instance>:EHICh:CARRier<Carrier>
	FETCh:WCDMa:SIGNaling<Instance>:EHICh:CARRier<Carrier>



.. autoclass:: RsCmwWcdmaSig.Implementations.Ehich_.Carrier.Carrier
	:members:
	:undoc-members:
	:noindex: