Hsupa
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:CARRier<Carrier>:HSUPa:ENABle

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:CARRier<Carrier>:HSUPa:ENABle



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Carrier_.Hsupa.Hsupa
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.carrier.hsupa.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cell_Carrier_Hsupa_Ehrch.rst
	Configure_Cell_Carrier_Hsupa_Eagch.rst
	Configure_Cell_Carrier_Hsupa_Ehich.rst
	Configure_Cell_Carrier_Hsupa_Ergch.rst
	Configure_Cell_Carrier_Hsupa_Etfci.rst