Carrier
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:HACK:MCQI:CARRier<Carrier>
	single: READ:WCDMa:SIGNaling<Instance>:HACK:MCQI:CARRier<Carrier>

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:HACK:MCQI:CARRier<Carrier>
	READ:WCDMa:SIGNaling<Instance>:HACK:MCQI:CARRier<Carrier>



.. autoclass:: RsCmwWcdmaSig.Implementations.Hack_.Mcqi_.Carrier.Carrier
	:members:
	:undoc-members:
	:noindex: