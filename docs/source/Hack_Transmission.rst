Transmission
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Hack_.Transmission.Transmission
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.hack.transmission.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Hack_Transmission_Carrier.rst