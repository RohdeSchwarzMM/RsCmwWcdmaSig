Inactivity
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Connection_.Packet_.Inactivity.Inactivity
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.packet.inactivity.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Packet_Inactivity_Dch.rst
	Configure_Connection_Packet_Inactivity_Fach.rst
	Configure_Connection_Packet_Inactivity_Cpch.rst
	Configure_Connection_Packet_Inactivity_Upch.rst