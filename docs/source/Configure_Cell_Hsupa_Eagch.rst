Eagch
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSUPa:EAGCh:TINDex
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSUPa:EAGCh:UTTI

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSUPa:EAGCh:TINDex
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSUPa:EAGCh:UTTI



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Hsupa_.Eagch.Eagch
	:members:
	:undoc-members:
	:noindex: