Fsimulator
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Sense_.Fading_.Carrier_.Fsimulator.Fsimulator
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.fading.carrier.fsimulator.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Fading_Carrier_Fsimulator_Iloss.rst