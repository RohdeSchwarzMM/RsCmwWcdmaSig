Level
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:LEVel:SCPich
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:LEVel:SCCPch
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:LEVel:PICH
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:LEVel:AICH
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:LEVel:DPCH

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:DL:LEVel:SCPich
	CONFigure:WCDMa:SIGNaling<Instance>:DL:LEVel:SCCPch
	CONFigure:WCDMa:SIGNaling<Instance>:DL:LEVel:PICH
	CONFigure:WCDMa:SIGNaling<Instance>:DL:LEVel:AICH
	CONFigure:WCDMa:SIGNaling<Instance>:DL:LEVel:DPCH



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Downlink_.Level.Level
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.downlink.level.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Downlink_Level_Adjust.rst