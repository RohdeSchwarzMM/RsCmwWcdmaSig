Thresholds
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:NCELl:LTE:THResholds:HIGH

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:NCELl:LTE:THResholds:HIGH



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Ncell_.Lte_.Thresholds.Thresholds
	:members:
	:undoc-members:
	:noindex: