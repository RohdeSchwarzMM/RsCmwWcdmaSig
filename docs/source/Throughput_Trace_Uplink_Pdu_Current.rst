Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:THRoughput:TRACe:UL:PDU:CURRent
	single: READ:WCDMa:SIGNaling<Instance>:THRoughput:TRACe:UL:PDU:CURRent

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:THRoughput:TRACe:UL:PDU:CURRent
	READ:WCDMa:SIGNaling<Instance>:THRoughput:TRACe:UL:PDU:CURRent



.. autoclass:: RsCmwWcdmaSig.Implementations.Throughput_.Trace_.Uplink_.Pdu_.Current.Current
	:members:
	:undoc-members:
	:noindex: