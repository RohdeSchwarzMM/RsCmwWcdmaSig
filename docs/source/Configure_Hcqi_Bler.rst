Bler
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:HCQI:BLER:MSFRames

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:HCQI:BLER:MSFRames



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Hcqi_.Bler.Bler
	:members:
	:undoc-members:
	:noindex: