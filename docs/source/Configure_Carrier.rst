Carrier
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CARRier<Carrier>:BAND

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CARRier<Carrier>:BAND



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Carrier.Carrier
	:members:
	:undoc-members:
	:noindex: