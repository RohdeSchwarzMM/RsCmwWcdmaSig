T<Timer>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: T313 .. T3312
	rc = driver.configure.cell.timeout.t.repcap_timer_get()
	driver.configure.cell.timeout.t.repcap_timer_set(repcap.Timer.T313)



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:TOUT:T<Timer>

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:TOUT:T<Timer>



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Timeout_.T.T
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.timeout.t.clone()