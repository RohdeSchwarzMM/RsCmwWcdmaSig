General
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:SIGNaling<Instance>:EAGCh:TRACe:GENeral
	single: FETCh:WCDMa:SIGNaling<Instance>:EAGCh:TRACe:GENeral

.. code-block:: python

	READ:WCDMa:SIGNaling<Instance>:EAGCh:TRACe:GENeral
	FETCh:WCDMa:SIGNaling<Instance>:EAGCh:TRACe:GENeral



.. autoclass:: RsCmwWcdmaSig.Implementations.Eagch_.Trace_.General.General
	:members:
	:undoc-members:
	:noindex: