Dch
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Connection_.Packet_.Inactivity_.Dch.Dch
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.packet.inactivity.dch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Packet_Inactivity_Dch_Network.rst
	Configure_Connection_Packet_Inactivity_Dch_UefDormancy.rst