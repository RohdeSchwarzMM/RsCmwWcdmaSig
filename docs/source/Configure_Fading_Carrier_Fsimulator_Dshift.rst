Dshift
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:FADing:CARRier<Carrier>:FSIMulator:DSHift:MODE
	single: CONFigure:WCDMa:SIGNaling<Instance>:FADing:CARRier<Carrier>:FSIMulator:DSHift

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:FADing:CARRier<Carrier>:FSIMulator:DSHift:MODE
	CONFigure:WCDMa:SIGNaling<Instance>:FADing:CARRier<Carrier>:FSIMulator:DSHift



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Fading_.Carrier_.Fsimulator_.Dshift.Dshift
	:members:
	:undoc-members:
	:noindex: