Hcqi
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:HCQI:TOUT
	single: CONFigure:WCDMa:SIGNaling<Instance>:HCQI:TCASe

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:HCQI:TOUT
	CONFigure:WCDMa:SIGNaling<Instance>:HCQI:TCASe



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Hcqi.Hcqi
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.hcqi.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Hcqi_Cqi.rst
	Configure_Hcqi_Bler.rst
	Configure_Hcqi_Limit.rst