State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:ERGCh:STATe

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:ERGCh:STATe



.. autoclass:: RsCmwWcdmaSig.Implementations.Ergch_.State.State
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ergch.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Ergch_State_All.rst