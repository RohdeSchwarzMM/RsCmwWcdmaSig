Hspa
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:TMODe:HSPA:PROCedure
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:TMODe:HSPA:DIRection
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:TMODe:HSPA:DATA
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:TMODe:HSPA:EINSertion
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:TMODe:HSPA:USDU

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:TMODe:HSPA:PROCedure
	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:TMODe:HSPA:DIRection
	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:TMODe:HSPA:DATA
	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:TMODe:HSPA:EINSertion
	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:TMODe:HSPA:USDU



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Connection_.Tmode_.Hspa.Hspa
	:members:
	:undoc-members:
	:noindex: