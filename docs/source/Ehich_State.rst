State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:EHICh:STATe

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:EHICh:STATe



.. autoclass:: RsCmwWcdmaSig.Implementations.Ehich_.State.State
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ehich.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Ehich_State_All.rst