Code
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:CODE:SCPich
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:CODE:PCCPch
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:CODE:SCCPch
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:CODE:PICH
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:CODE:AICH
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:CODE:DPCH
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:CODE:FDPCh

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:DL:CODE:SCPich
	CONFigure:WCDMa:SIGNaling<Instance>:DL:CODE:PCCPch
	CONFigure:WCDMa:SIGNaling<Instance>:DL:CODE:SCCPch
	CONFigure:WCDMa:SIGNaling<Instance>:DL:CODE:PICH
	CONFigure:WCDMa:SIGNaling<Instance>:DL:CODE:AICH
	CONFigure:WCDMa:SIGNaling<Instance>:DL:CODE:DPCH
	CONFigure:WCDMa:SIGNaling<Instance>:DL:CODE:FDPCh



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Downlink_.Code.Code
	:members:
	:undoc-members:
	:noindex: