Etfci
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:ULLogging:CARRier<Carrier>:ETFCi
	single: READ:WCDMa:SIGNaling<Instance>:ULLogging:CARRier<Carrier>:ETFCi

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:ULLogging:CARRier<Carrier>:ETFCi
	READ:WCDMa:SIGNaling<Instance>:ULLogging:CARRier<Carrier>:ETFCi



.. autoclass:: RsCmwWcdmaSig.Implementations.UplinkLogging_.Carrier_.Etfci.Etfci
	:members:
	:undoc-members:
	:noindex: