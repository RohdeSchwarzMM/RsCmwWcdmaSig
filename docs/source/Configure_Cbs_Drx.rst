Drx
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CBS:DRX:ENABle
	single: CONFigure:WCDMa:SIGNaling<Instance>:CBS:DRX:PERiod
	single: CONFigure:WCDMa:SIGNaling<Instance>:CBS:DRX:LENGth
	single: CONFigure:WCDMa:SIGNaling<Instance>:CBS:DRX:OFFSet
	single: CONFigure:WCDMa:SIGNaling<Instance>:CBS:DRX:FEMPty

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CBS:DRX:ENABle
	CONFigure:WCDMa:SIGNaling<Instance>:CBS:DRX:PERiod
	CONFigure:WCDMa:SIGNaling<Instance>:CBS:DRX:LENGth
	CONFigure:WCDMa:SIGNaling<Instance>:CBS:DRX:OFFSet
	CONFigure:WCDMa:SIGNaling<Instance>:CBS:DRX:FEMPty



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cbs_.Drx.Drx
	:members:
	:undoc-members:
	:noindex: