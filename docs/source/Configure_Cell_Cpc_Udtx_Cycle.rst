Cycle<Cycle>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.configure.cell.cpc.udtx.cycle.repcap_cycle_get()
	driver.configure.cell.cpc.udtx.cycle.repcap_cycle_set(repcap.Cycle.Nr1)





.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Cpc_.Udtx_.Cycle.Cycle
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.cpc.udtx.cycle.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cell_Cpc_Udtx_Cycle_Apattern.rst
	Configure_Cell_Cpc_Udtx_Cycle_Burst.rst
	Configure_Cell_Cpc_Udtx_Cycle_Ithreshold.rst
	Configure_Cell_Cpc_Udtx_Cycle_Dsg.rst