CoPower
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:COPower:TOTal

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:COPower:TOTal



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.RfSettings_.CoPower.CoPower
	:members:
	:undoc-members:
	:noindex: