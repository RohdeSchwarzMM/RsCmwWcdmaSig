Qpsk
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSDPa:CQI:RVCSequences:QPSK:UDEFined
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSDPa:CQI:RVCSequences:QPSK

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSDPa:CQI:RVCSequences:QPSK:UDEFined
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSDPa:CQI:RVCSequences:QPSK



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Hsdpa_.Cqi_.RvcSequences_.Qpsk.Qpsk
	:members:
	:undoc-members:
	:noindex: