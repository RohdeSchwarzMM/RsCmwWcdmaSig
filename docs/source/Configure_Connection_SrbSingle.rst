SrbSingle
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:SRBSingle:TYPE

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:SRBSingle:TYPE



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Connection_.SrbSingle.SrbSingle
	:members:
	:undoc-members:
	:noindex: