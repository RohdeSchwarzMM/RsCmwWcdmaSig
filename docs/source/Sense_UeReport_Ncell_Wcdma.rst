Wcdma
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Sense_.UeReport_.Ncell_.Wcdma.Wcdma
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ueReport.ncell.wcdma.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UeReport_Ncell_Wcdma_Cell.rst