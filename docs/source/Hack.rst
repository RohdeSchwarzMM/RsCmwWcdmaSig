Hack
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STOP:WCDMa:SIGNaling<Instance>:HACK
	single: ABORt:WCDMa:SIGNaling<Instance>:HACK
	single: INITiate:WCDMa:SIGNaling<Instance>:HACK

.. code-block:: python

	STOP:WCDMa:SIGNaling<Instance>:HACK
	ABORt:WCDMa:SIGNaling<Instance>:HACK
	INITiate:WCDMa:SIGNaling<Instance>:HACK



.. autoclass:: RsCmwWcdmaSig.Implementations.Hack.Hack
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.hack.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Hack_Trace.rst
	Hack_Mcqi.rst
	Hack_MsFrames.rst
	Hack_Bler.rst
	Hack_Throughput.rst
	Hack_Transmission.rst
	Hack_State.rst