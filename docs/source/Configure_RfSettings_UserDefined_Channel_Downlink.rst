Downlink
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:UDEFined:CHANnel:DL:MINimum
	single: CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:UDEFined:CHANnel:DL:MAXimum

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:UDEFined:CHANnel:DL:MINimum
	CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:UDEFined:CHANnel:DL:MAXimum



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.RfSettings_.UserDefined_.Channel_.Downlink.Downlink
	:members:
	:undoc-members:
	:noindex: