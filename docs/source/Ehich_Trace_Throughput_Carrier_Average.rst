Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:SIGNaling<Instance>:EHICh:TRACe:THRoughput:CARRier<Carrier>:AVERage
	single: FETCh:WCDMa:SIGNaling<Instance>:EHICh:TRACe:THRoughput:CARRier<Carrier>:AVERage

.. code-block:: python

	READ:WCDMa:SIGNaling<Instance>:EHICh:TRACe:THRoughput:CARRier<Carrier>:AVERage
	FETCh:WCDMa:SIGNaling<Instance>:EHICh:TRACe:THRoughput:CARRier<Carrier>:AVERage



.. autoclass:: RsCmwWcdmaSig.Implementations.Ehich_.Trace_.Throughput_.Carrier_.Average.Average
	:members:
	:undoc-members:
	:noindex: