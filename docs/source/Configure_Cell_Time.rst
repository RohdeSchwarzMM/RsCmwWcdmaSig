Time
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:TIME:TSOurce
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:TIME:DATE
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:TIME:TIME
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:TIME:DSTime
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:TIME:LTZoffset
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:TIME:SREGister
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:TIME:SNName

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:TIME:TSOurce
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:TIME:DATE
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:TIME:TIME
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:TIME:DSTime
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:TIME:LTZoffset
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:TIME:SREGister
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:TIME:SNName



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Time.Time
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.time.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cell_Time_Snow.rst