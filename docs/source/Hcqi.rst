Hcqi
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STOP:WCDMa:SIGNaling<Instance>:HCQI
	single: ABORt:WCDMa:SIGNaling<Instance>:HCQI
	single: INITiate:WCDMa:SIGNaling<Instance>:HCQI

.. code-block:: python

	STOP:WCDMa:SIGNaling<Instance>:HCQI
	ABORt:WCDMa:SIGNaling<Instance>:HCQI
	INITiate:WCDMa:SIGNaling<Instance>:HCQI



.. autoclass:: RsCmwWcdmaSig.Implementations.Hcqi.Hcqi
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.hcqi.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Hcqi_State.rst
	Hcqi_Rstate.rst
	Hcqi_Carrier.rst
	Hcqi_Trace.rst