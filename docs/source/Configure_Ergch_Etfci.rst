Etfci
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:ERGCh:ETFCi:MODE
	single: CONFigure:WCDMa:SIGNaling<Instance>:ERGCh:ETFCi:EXPected
	single: CONFigure:WCDMa:SIGNaling<Instance>:ERGCh:ETFCi:INITial
	single: CONFigure:WCDMa:SIGNaling<Instance>:ERGCh:ETFCi:MANual
	single: CONFigure:WCDMa:SIGNaling<Instance>:ERGCh:ETFCi:AUTO

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:ERGCh:ETFCi:MODE
	CONFigure:WCDMa:SIGNaling<Instance>:ERGCh:ETFCi:EXPected
	CONFigure:WCDMa:SIGNaling<Instance>:ERGCh:ETFCi:INITial
	CONFigure:WCDMa:SIGNaling<Instance>:ERGCh:ETFCi:MANual
	CONFigure:WCDMa:SIGNaling<Instance>:ERGCh:ETFCi:AUTO



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Ergch_.Etfci.Etfci
	:members:
	:undoc-members:
	:noindex: