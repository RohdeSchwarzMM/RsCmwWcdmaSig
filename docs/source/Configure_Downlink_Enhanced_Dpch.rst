Dpch
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:ENHanced:DPCH:RXLStrategy
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:ENHanced:DPCH:PHASe
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:ENHanced:DPCH:SSCode
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:ENHanced:DPCH:TOFFset
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:ENHanced:DPCH:RANGe

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:DL:ENHanced:DPCH:RXLStrategy
	CONFigure:WCDMa:SIGNaling<Instance>:DL:ENHanced:DPCH:PHASe
	CONFigure:WCDMa:SIGNaling<Instance>:DL:ENHanced:DPCH:SSCode
	CONFigure:WCDMa:SIGNaling<Instance>:DL:ENHanced:DPCH:TOFFset
	CONFigure:WCDMa:SIGNaling<Instance>:DL:ENHanced:DPCH:RANGe



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Downlink_.Enhanced_.Dpch.Dpch
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.downlink.enhanced.dpch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Downlink_Enhanced_Dpch_Lsequence.rst