Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:THRoughput:TRACe:UL:PDU:AVERage
	single: READ:WCDMa:SIGNaling<Instance>:THRoughput:TRACe:UL:PDU:AVERage

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:THRoughput:TRACe:UL:PDU:AVERage
	READ:WCDMa:SIGNaling<Instance>:THRoughput:TRACe:UL:PDU:AVERage



.. autoclass:: RsCmwWcdmaSig.Implementations.Throughput_.Trace_.Uplink_.Pdu_.Average.Average
	:members:
	:undoc-members:
	:noindex: