Request
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:REQuest:IMEI
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:REQuest:ADETach
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:REQuest:RCUR

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:REQuest:IMEI
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:REQuest:ADETach
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:REQuest:RCUR



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Request.Request
	:members:
	:undoc-members:
	:noindex: