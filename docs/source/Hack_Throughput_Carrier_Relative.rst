Relative
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:HACK:THRoughput:CARRier<Carrier>:RELative
	single: READ:WCDMa:SIGNaling<Instance>:HACK:THRoughput:CARRier<Carrier>:RELative

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:HACK:THRoughput:CARRier<Carrier>:RELative
	READ:WCDMa:SIGNaling<Instance>:HACK:THRoughput:CARRier<Carrier>:RELative



.. autoclass:: RsCmwWcdmaSig.Implementations.Hack_.Throughput_.Carrier_.Relative.Relative
	:members:
	:undoc-members:
	:noindex: