Flexible
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:WCDMa:SIGNaling<Instance>:SCENario:DCFDiversity:FLEXible:EXTernal
	single: ROUTe:WCDMa:SIGNaling<Instance>:SCENario:DCFDiversity:FLEXible:INTernal

.. code-block:: python

	ROUTe:WCDMa:SIGNaling<Instance>:SCENario:DCFDiversity:FLEXible:EXTernal
	ROUTe:WCDMa:SIGNaling<Instance>:SCENario:DCFDiversity:FLEXible:INTernal



.. autoclass:: RsCmwWcdmaSig.Implementations.Route_.Scenario_.DcfDiversity_.Flexible.Flexible
	:members:
	:undoc-members:
	:noindex: