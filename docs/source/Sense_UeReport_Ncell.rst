Ncell
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:WCDMa:SIGNaling<Instance>:UEReport:NCELl<DownCarrier>

.. code-block:: python

	SENSe:WCDMa:SIGNaling<Instance>:UEReport:NCELl<DownCarrier>



.. autoclass:: RsCmwWcdmaSig.Implementations.Sense_.UeReport_.Ncell.Ncell
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ueReport.ncell.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UeReport_Ncell_Gsm.rst
	Sense_UeReport_Ncell_Wcdma.rst
	Sense_UeReport_Ncell_Lte.rst