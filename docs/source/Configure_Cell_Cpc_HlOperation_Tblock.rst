Tblock<TransportBlock>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: TBl1 .. TBl4
	rc = driver.configure.cell.cpc.hlOperation.tblock.repcap_transportBlock_get()
	driver.configure.cell.cpc.hlOperation.tblock.repcap_transportBlock_set(repcap.TransportBlock.TBl1)



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:HLOPeration:TBLock<TransportBlock>

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:HLOPeration:TBLock<TransportBlock>



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Cpc_.HlOperation_.Tblock.Tblock
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.cpc.hlOperation.tblock.clone()