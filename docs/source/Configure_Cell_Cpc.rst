Cpc
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:SFORmat

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:SFORmat



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Cpc.Cpc
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.cpc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cell_Cpc_Dtrx.rst
	Configure_Cell_Cpc_Udtx.rst
	Configure_Cell_Cpc_Ddrx.rst
	Configure_Cell_Cpc_Mac.rst
	Configure_Cell_Cpc_HlOperation.rst
	Configure_Cell_Cpc_Horder.rst