UserDefined
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSDPa:UDEFined:RVCSequences:QAM<QuadratureAM>:UDEFined

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSDPa:UDEFined:RVCSequences:QAM<QuadratureAM>:UDEFined



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Hsdpa_.UserDefined_.RvcSequences_.Qam_.UserDefined.UserDefined
	:members:
	:undoc-members:
	:noindex: