Ncell
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:UEReport:NCELl:ENABle

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:UEReport:NCELl:ENABle



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.UeReport_.Ncell.Ncell
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.ueReport.ncell.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_UeReport_Ncell_Gsm.rst
	Configure_UeReport_Ncell_Wcdma.rst
	Configure_UeReport_Ncell_Lte.rst