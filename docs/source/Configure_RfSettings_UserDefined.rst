UserDefined
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:UDEFined:UDSeparation

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:UDEFined:UDSeparation



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.RfSettings_.UserDefined.UserDefined
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.userDefined.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_UserDefined_Channel.rst
	Configure_RfSettings_UserDefined_Frequency.rst