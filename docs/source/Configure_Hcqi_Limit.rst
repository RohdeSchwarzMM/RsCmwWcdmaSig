Limit
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Hcqi_.Limit.Limit
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.hcqi.limit.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Hcqi_Limit_Awgn.rst
	Configure_Hcqi_Limit_Fading.rst