IdDummy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:HSSCch<HSSCch>:IDDummy

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:HSSCch<HSSCch>:IDDummy



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Downlink_.Carrier_.Hsscch_.IdDummy.IdDummy
	:members:
	:undoc-members:
	:noindex: