Cqi
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:ULLogging:CARRier<Carrier>:CQI
	single: READ:WCDMa:SIGNaling<Instance>:ULLogging:CARRier<Carrier>:CQI

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:ULLogging:CARRier<Carrier>:CQI
	READ:WCDMa:SIGNaling<Instance>:ULLogging:CARRier<Carrier>:CQI



.. autoclass:: RsCmwWcdmaSig.Implementations.UplinkLogging_.Carrier_.Cqi.Cqi
	:members:
	:undoc-members:
	:noindex: