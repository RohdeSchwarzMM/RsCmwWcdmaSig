Foffset
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:CARRier<Carrier>:FOFFset:UL
	single: CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:CARRier<Carrier>:FOFFset:DL

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:CARRier<Carrier>:FOFFset:UL
	CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:CARRier<Carrier>:FOFFset:DL



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.RfSettings_.Carrier_.Foffset.Foffset
	:members:
	:undoc-members:
	:noindex: