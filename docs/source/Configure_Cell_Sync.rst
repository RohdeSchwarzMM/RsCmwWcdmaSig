Sync
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:SYNC:ZONE
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:SYNC:OFFSet

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:SYNC:ZONE
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:SYNC:OFFSet



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Sync.Sync
	:members:
	:undoc-members:
	:noindex: