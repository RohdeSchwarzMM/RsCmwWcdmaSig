Cell
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:NCELl:LTE:CELL<Cell>

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:NCELl:LTE:CELL<Cell>



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Ncell_.Lte_.Cell.Cell
	:members:
	:undoc-members:
	:noindex: