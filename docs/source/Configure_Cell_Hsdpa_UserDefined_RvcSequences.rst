RvcSequences
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Hsdpa_.UserDefined_.RvcSequences.RvcSequences
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.hsdpa.userDefined.rvcSequences.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cell_Hsdpa_UserDefined_RvcSequences_Qpsk.rst
	Configure_Cell_Hsdpa_UserDefined_RvcSequences_Qam.rst