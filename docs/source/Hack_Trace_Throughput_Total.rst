Total
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Hack_.Trace_.Throughput_.Total.Total
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.hack.trace.throughput.total.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Hack_Trace_Throughput_Total_Average.rst
	Hack_Trace_Throughput_Total_Current.rst