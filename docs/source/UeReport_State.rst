State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:UEReport:STATe

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:UEReport:STATe



.. autoclass:: RsCmwWcdmaSig.Implementations.UeReport_.State.State
	:members:
	:undoc-members:
	:noindex: