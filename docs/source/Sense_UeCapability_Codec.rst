Codec
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:WCDMa:SIGNaling<Instance>:UECapability:CODec:GSM
	single: SENSe:WCDMa:SIGNaling<Instance>:UECapability:CODec:UMTS

.. code-block:: python

	SENSe:WCDMa:SIGNaling<Instance>:UECapability:CODec:GSM
	SENSe:WCDMa:SIGNaling<Instance>:UECapability:CODec:UMTS



.. autoclass:: RsCmwWcdmaSig.Implementations.Sense_.UeCapability_.Codec.Codec
	:members:
	:undoc-members:
	:noindex: