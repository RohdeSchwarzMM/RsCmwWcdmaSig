IhMobility
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:IHMobility:HANDover
	single: CONFigure:WCDMa:SIGNaling<Instance>:IHMobility:MTCS

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:IHMobility:HANDover
	CONFigure:WCDMa:SIGNaling<Instance>:IHMobility:MTCS



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.IhMobility.IhMobility
	:members:
	:undoc-members:
	:noindex: