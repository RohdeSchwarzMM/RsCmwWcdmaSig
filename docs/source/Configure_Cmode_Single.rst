Single
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CMODe:SINGle:TYPE
	single: CONFigure:WCDMa:SIGNaling<Instance>:CMODe:SINGle:ACTivation

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CMODe:SINGle:TYPE
	CONFigure:WCDMa:SIGNaling<Instance>:CMODe:SINGle:ACTivation



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cmode_.Single.Single
	:members:
	:undoc-members:
	:noindex: