Gsm
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:WCDMa:SIGNaling<Instance>:UECapability:MEASurement:CMODe:GSM

.. code-block:: python

	SENSe:WCDMa:SIGNaling<Instance>:UECapability:MEASurement:CMODe:GSM



.. autoclass:: RsCmwWcdmaSig.Implementations.Sense_.UeCapability_.Measurement_.Cmode_.Gsm.Gsm
	:members:
	:undoc-members:
	:noindex: