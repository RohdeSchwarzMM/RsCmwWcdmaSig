Dcarrier
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:WCDMa:SIGNaling<Instance>:SCENario:DCARrier:FLEXible
	single: ROUTe:WCDMa:SIGNaling<Instance>:SCENario:DCARrier

.. code-block:: python

	ROUTe:WCDMa:SIGNaling<Instance>:SCENario:DCARrier:FLEXible
	ROUTe:WCDMa:SIGNaling<Instance>:SCENario:DCARrier



.. autoclass:: RsCmwWcdmaSig.Implementations.Route_.Scenario_.Dcarrier.Dcarrier
	:members:
	:undoc-members:
	:noindex: