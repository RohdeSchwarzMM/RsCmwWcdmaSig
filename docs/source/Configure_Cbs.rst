Cbs
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cbs.Cbs
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cbs.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cbs_Ctch.rst
	Configure_Cbs_Drx.rst
	Configure_Cbs_Message.rst