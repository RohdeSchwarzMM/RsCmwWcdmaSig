N<CounterNo>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr313 .. Nr313
	rc = driver.configure.cell.timeout.n.repcap_counterNo_get()
	driver.configure.cell.timeout.n.repcap_counterNo_set(repcap.CounterNo.Nr313)



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:TOUT:N<CounterNo>

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:TOUT:N<CounterNo>



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Timeout_.N.N
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.timeout.n.clone()