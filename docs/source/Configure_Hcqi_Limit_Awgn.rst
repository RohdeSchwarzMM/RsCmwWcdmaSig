Awgn
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:HCQI:LIMit:AWGN:BLER
	single: CONFigure:WCDMa:SIGNaling<Instance>:HCQI:LIMit:AWGN:DTX
	single: CONFigure:WCDMa:SIGNaling<Instance>:HCQI:LIMit:AWGN

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:HCQI:LIMit:AWGN:BLER
	CONFigure:WCDMa:SIGNaling<Instance>:HCQI:LIMit:AWGN:DTX
	CONFigure:WCDMa:SIGNaling<Instance>:HCQI:LIMit:AWGN



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Hcqi_.Limit_.Awgn.Awgn
	:members:
	:undoc-members:
	:noindex: