Cell
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:NCELl:GSM:CELL<Cell>

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:NCELl:GSM:CELL<Cell>



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Ncell_.Gsm_.Cell.Cell
	:members:
	:undoc-members:
	:noindex: