Message
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CBS:MESSage:ENABle
	single: CONFigure:WCDMa:SIGNaling<Instance>:CBS:MESSage:ID
	single: CONFigure:WCDMa:SIGNaling<Instance>:CBS:MESSage:IDTYpe
	single: CONFigure:WCDMa:SIGNaling<Instance>:CBS:MESSage:SERial
	single: CONFigure:WCDMa:SIGNaling<Instance>:CBS:MESSage:CGRoup
	single: CONFigure:WCDMa:SIGNaling<Instance>:CBS:MESSage:CATegory
	single: CONFigure:WCDMa:SIGNaling<Instance>:CBS:MESSage:SOURce
	single: CONFigure:WCDMa:SIGNaling<Instance>:CBS:MESSage:DATA
	single: CONFigure:WCDMa:SIGNaling<Instance>:CBS:MESSage:PERiod

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CBS:MESSage:ENABle
	CONFigure:WCDMa:SIGNaling<Instance>:CBS:MESSage:ID
	CONFigure:WCDMa:SIGNaling<Instance>:CBS:MESSage:IDTYpe
	CONFigure:WCDMa:SIGNaling<Instance>:CBS:MESSage:SERial
	CONFigure:WCDMa:SIGNaling<Instance>:CBS:MESSage:CGRoup
	CONFigure:WCDMa:SIGNaling<Instance>:CBS:MESSage:CATegory
	CONFigure:WCDMa:SIGNaling<Instance>:CBS:MESSage:SOURce
	CONFigure:WCDMa:SIGNaling<Instance>:CBS:MESSage:DATA
	CONFigure:WCDMa:SIGNaling<Instance>:CBS:MESSage:PERiod



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cbs_.Message.Message
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cbs.message.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cbs_Message_Language.rst
	Configure_Cbs_Message_File.rst
	Configure_Cbs_Message_Etws.rst