State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:EAGCh:STATe

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:EAGCh:STATe



.. autoclass:: RsCmwWcdmaSig.Implementations.Eagch_.State.State
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.eagch.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Eagch_State_All.rst