Rmc
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:TMODe:RMC:DOMain
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:TMODe:RMC:DATA
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:TMODe:RMC:DLRessources
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:TMODe:RMC:UCRC
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:TMODe:RMC:RLCMode
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:TMODe:RMC:DRATe
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:TMODe:RMC:TMODe

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:TMODe:RMC:DOMain
	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:TMODe:RMC:DATA
	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:TMODe:RMC:DLRessources
	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:TMODe:RMC:UCRC
	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:TMODe:RMC:RLCMode
	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:TMODe:RMC:DRATe
	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:TMODe:RMC:TMODe



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Connection_.Tmode_.Rmc.Rmc
	:members:
	:undoc-members:
	:noindex: