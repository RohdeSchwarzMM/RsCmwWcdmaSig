Wcdma
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:WCDMa:SIGNaling<Instance>:UECapability:MEASurement:CMODe:WCDMa

.. code-block:: python

	SENSe:WCDMa:SIGNaling<Instance>:UECapability:MEASurement:CMODe:WCDMa



.. autoclass:: RsCmwWcdmaSig.Implementations.Sense_.UeCapability_.Measurement_.Cmode_.Wcdma.Wcdma
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ueCapability.measurement.cmode.wcdma.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UeCapability_Measurement_Cmode_Wcdma_Mcarrier.rst