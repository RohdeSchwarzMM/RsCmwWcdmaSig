Band<Band>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: B1 .. B32
	rc = driver.sense.ueCapability.rfParameter.band.repcap_band_get()
	driver.sense.ueCapability.rfParameter.band.repcap_band_set(repcap.Band.B1)



.. rubric:: SCPI Commands

.. index::
	single: SENSe:WCDMa:SIGNaling<Instance>:UECapability:RFParameter:BAND<Band>

.. code-block:: python

	SENSe:WCDMa:SIGNaling<Instance>:UECapability:RFParameter:BAND<Band>



.. autoclass:: RsCmwWcdmaSig.Implementations.Sense_.UeCapability_.RfParameter_.Band.Band
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ueCapability.rfParameter.band.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UeCapability_RfParameter_Band_Nc.rst