Tmode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:TMODe:KTLReconfig
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:TMODe:TYPE

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:TMODe:KTLReconfig
	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:TMODe:TYPE



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Connection_.Tmode.Tmode
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.tmode.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Tmode_Btfd.rst
	Configure_Connection_Tmode_Rmc.rst
	Configure_Connection_Tmode_Hspa.rst