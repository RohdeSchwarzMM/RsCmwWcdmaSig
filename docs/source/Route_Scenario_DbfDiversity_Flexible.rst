Flexible
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:WCDMa:SIGNaling<Instance>:SCENario:DBFDiversity:FLEXible:EXTernal
	single: ROUTe:WCDMa:SIGNaling<Instance>:SCENario:DBFDiversity:FLEXible:INTernal

.. code-block:: python

	ROUTe:WCDMa:SIGNaling<Instance>:SCENario:DBFDiversity:FLEXible:EXTernal
	ROUTe:WCDMa:SIGNaling<Instance>:SCENario:DBFDiversity:FLEXible:INTernal



.. autoclass:: RsCmwWcdmaSig.Implementations.Route_.Scenario_.DbfDiversity_.Flexible.Flexible
	:members:
	:undoc-members:
	:noindex: