Prach
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:PRACh:DRXCycle

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:UL:PRACh:DRXCycle



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Uplink_.Prach.Prach
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.uplink.prach.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Uplink_Prach_Preamble.rst
	Configure_Uplink_Prach_Message.rst