Message
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:PRACh:MESSage:POFFset
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:PRACh:MESSage:LENGth

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:UL:PRACh:MESSage:POFFset
	CONFigure:WCDMa:SIGNaling<Instance>:UL:PRACh:MESSage:LENGth



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Uplink_.Prach_.Message.Message
	:members:
	:undoc-members:
	:noindex: