Dpch
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:ENHanced:DPCH:REPorted

.. code-block:: python

	SENSe:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:ENHanced:DPCH:REPorted



.. autoclass:: RsCmwWcdmaSig.Implementations.Sense_.Downlink_.Carrier_.Enhanced_.Dpch.Dpch
	:members:
	:undoc-members:
	:noindex: