Connection
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Clean_.Connection.Connection
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.clean.connection.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Clean_Connection_Cswitched.rst