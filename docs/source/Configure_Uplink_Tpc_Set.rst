Set
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:TPC:SET

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:UL:TPC:SET



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Uplink_.Tpc_.Set.Set
	:members:
	:undoc-members:
	:noindex: