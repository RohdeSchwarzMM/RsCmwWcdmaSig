Flexible
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:WCDMa:SIGNaling<Instance>:SCENario:SCFDiversity:FLEXible:EXTernal
	single: ROUTe:WCDMa:SIGNaling<Instance>:SCENario:SCFDiversity:FLEXible:INTernal

.. code-block:: python

	ROUTe:WCDMa:SIGNaling<Instance>:SCENario:SCFDiversity:FLEXible:EXTernal
	ROUTe:WCDMa:SIGNaling<Instance>:SCENario:SCFDiversity:FLEXible:INTernal



.. autoclass:: RsCmwWcdmaSig.Implementations.Route_.Scenario_.ScfDiversity_.Flexible.Flexible
	:members:
	:undoc-members:
	:noindex: