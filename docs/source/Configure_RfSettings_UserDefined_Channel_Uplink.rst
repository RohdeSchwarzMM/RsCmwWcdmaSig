Uplink
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:UDEFined:CHANnel:UL:MINimum
	single: CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:UDEFined:CHANnel:UL:MAXimum

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:UDEFined:CHANnel:UL:MINimum
	CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:UDEFined:CHANnel:UL:MAXimum



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.RfSettings_.UserDefined_.Channel_.Uplink.Uplink
	:members:
	:undoc-members:
	:noindex: