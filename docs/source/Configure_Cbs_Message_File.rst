File
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CBS:MESSage:FILE:INFO
	single: CONFigure:WCDMa:SIGNaling<Instance>:CBS:MESSage:FILE

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CBS:MESSage:FILE:INFO
	CONFigure:WCDMa:SIGNaling<Instance>:CBS:MESSage:FILE



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cbs_.Message_.File.File
	:members:
	:undoc-members:
	:noindex: