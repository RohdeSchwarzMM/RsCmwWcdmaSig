UePosition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:WCDMa:SIGNaling<Instance>:UECapability:UEPosition

.. code-block:: python

	SENSe:WCDMa:SIGNaling<Instance>:UECapability:UEPosition



.. autoclass:: RsCmwWcdmaSig.Implementations.Sense_.UeCapability_.UePosition.UePosition
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ueCapability.uePosition.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UeCapability_UePosition_Ganss.rst