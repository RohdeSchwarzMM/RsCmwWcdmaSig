Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:SIGNaling<Instance>:HACK:TRACe:SUBFrame:CARRier<Carrier>:CODE:MINimum
	single: FETCh:WCDMa:SIGNaling<Instance>:HACK:TRACe:SUBFrame:CARRier<Carrier>:CODE:MINimum

.. code-block:: python

	READ:WCDMa:SIGNaling<Instance>:HACK:TRACe:SUBFrame:CARRier<Carrier>:CODE:MINimum
	FETCh:WCDMa:SIGNaling<Instance>:HACK:TRACe:SUBFrame:CARRier<Carrier>:CODE:MINimum



.. autoclass:: RsCmwWcdmaSig.Implementations.Hack_.Trace_.Subframe_.Carrier_.Code_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: