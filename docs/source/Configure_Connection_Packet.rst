Packet
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:PACKet:DRATe

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:PACKet:DRATe



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Connection_.Packet.Packet
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.packet.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Packet_Hsdpa.rst
	Configure_Connection_Packet_Inactivity.rst
	Configure_Connection_Packet_Rohc.rst