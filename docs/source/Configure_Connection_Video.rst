Video
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:VIDeo:DRATe

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:VIDeo:DRATe



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Connection_.Video.Video
	:members:
	:undoc-members:
	:noindex: