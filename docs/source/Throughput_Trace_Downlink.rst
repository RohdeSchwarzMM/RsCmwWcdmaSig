Downlink
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Throughput_.Trace_.Downlink.Downlink
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.throughput.trace.downlink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Throughput_Trace_Downlink_Sdu.rst
	Throughput_Trace_Downlink_Pdu.rst