UeReport
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:UEReport:ENABle
	single: CONFigure:WCDMa:SIGNaling<Instance>:UEReport:RINTerval

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:UEReport:ENABle
	CONFigure:WCDMa:SIGNaling<Instance>:UEReport:RINTerval



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.UeReport.UeReport
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.ueReport.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_UeReport_Ccell.rst
	Configure_UeReport_Ncell.rst