Connection
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:WCDMa:SIGNaling<Instance>:UESinfo:CONNection:PACKet
	single: SENSe:WCDMa:SIGNaling<Instance>:UESinfo:CONNection:CIRCuit

.. code-block:: python

	SENSe:WCDMa:SIGNaling<Instance>:UESinfo:CONNection:PACKet
	SENSe:WCDMa:SIGNaling<Instance>:UESinfo:CONNection:CIRCuit



.. autoclass:: RsCmwWcdmaSig.Implementations.Sense_.UesInfo_.Connection.Connection
	:members:
	:undoc-members:
	:noindex: