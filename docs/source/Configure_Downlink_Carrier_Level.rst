Level
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:LEVel:APOWer
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:LEVel:EHICh
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:LEVel:ERGCh
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:LEVel:EAGCh
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:LEVel:HSPDsch
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:LEVel:PSCH
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:LEVel:SSCH
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:LEVel:PCPich
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:LEVel:PCCPch
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:LEVel:FDPCh

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:LEVel:APOWer
	CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:LEVel:EHICh
	CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:LEVel:ERGCh
	CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:LEVel:EAGCh
	CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:LEVel:HSPDsch
	CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:LEVel:PSCH
	CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:LEVel:SSCH
	CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:LEVel:PCPich
	CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:LEVel:PCCPch
	CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:LEVel:FDPCh



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Downlink_.Carrier_.Level.Level
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.downlink.carrier.level.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Downlink_Carrier_Level_Hsscch.rst