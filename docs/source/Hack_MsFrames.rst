MsFrames
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:HACK:MSFRames
	single: READ:WCDMa:SIGNaling<Instance>:HACK:MSFRames

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:HACK:MSFRames
	READ:WCDMa:SIGNaling<Instance>:HACK:MSFRames



.. autoclass:: RsCmwWcdmaSig.Implementations.Hack_.MsFrames.MsFrames
	:members:
	:undoc-members:
	:noindex: