Configure
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:ETOE
	single: CONFigure:WCDMa:SIGNaling<Instance>:ESCode

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:ETOE
	CONFigure:WCDMa:SIGNaling<Instance>:ESCode



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure.Configure
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Psettings.rst
	Configure_Mmonitor.rst
	Configure_UeReport.rst
	Configure_Cmode.rst
	Configure_RfSettings.rst
	Configure_Carrier.rst
	Configure_IqIn.rst
	Configure_Downlink.rst
	Configure_Uplink.rst
	Configure_Connection.rst
	Configure_IhMobility.rst
	Configure_Cell.rst
	Configure_Ncell.rst
	Configure_Ber.rst
	Configure_Throughput.rst
	Configure_Hack.rst
	Configure_Hcqi.rst
	Configure_UplinkLogging.rst
	Configure_Eagch.rst
	Configure_Ehich.rst
	Configure_Ergch.rst
	Configure_Sms.rst
	Configure_Cbs.rst
	Configure_Fading.rst