Wcdma
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Ncell_.Wcdma.Wcdma
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.ncell.wcdma.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Ncell_Wcdma_Cell.rst