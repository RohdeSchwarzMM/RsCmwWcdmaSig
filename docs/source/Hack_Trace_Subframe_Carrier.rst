Carrier
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Hack_.Trace_.Subframe_.Carrier.Carrier
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.hack.trace.subframe.carrier.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Hack_Trace_Subframe_Carrier_Tblock.rst
	Hack_Trace_Subframe_Carrier_Code.rst
	Hack_Trace_Subframe_Carrier_Modulation.rst