Throughput
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Ehich_.Trace_.Throughput.Throughput
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ehich.trace.throughput.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Ehich_Trace_Throughput_Carrier.rst