Awgn
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:FADing:CARRier<Carrier>:AWGN:NOISe
	single: CONFigure:WCDMa:SIGNaling<Instance>:FADing:CARRier<Carrier>:AWGN:ENABle
	single: CONFigure:WCDMa:SIGNaling<Instance>:FADing:CARRier<Carrier>:AWGN:SNRatio

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:FADing:CARRier<Carrier>:AWGN:NOISe
	CONFigure:WCDMa:SIGNaling<Instance>:FADing:CARRier<Carrier>:AWGN:ENABle
	CONFigure:WCDMa:SIGNaling<Instance>:FADing:CARRier<Carrier>:AWGN:SNRatio



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Fading_.Carrier_.Awgn.Awgn
	:members:
	:undoc-members:
	:noindex: