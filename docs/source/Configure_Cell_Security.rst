Security
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:SECurity:CIPHering
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:SECurity:OPC
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:SECurity:SIMCard
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:SECurity:SKEY
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:SECurity:ENABle
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:SECurity:AUTHenticat

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:SECurity:CIPHering
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:SECurity:OPC
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:SECurity:SIMCard
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:SECurity:SKEY
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:SECurity:ENABle
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:SECurity:AUTHenticat



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Security.Security
	:members:
	:undoc-members:
	:noindex: