Precondition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:TPC:PRECondition

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:UL:TPC:PRECondition



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Uplink_.Tpc_.Precondition.Precondition
	:members:
	:undoc-members:
	:noindex: