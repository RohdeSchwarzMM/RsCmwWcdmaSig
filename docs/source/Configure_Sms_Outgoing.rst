Outgoing
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:SMS:OUTGoing:BINary
	single: CONFigure:WCDMa:SIGNaling<Instance>:SMS:OUTGoing:PIDentifier
	single: CONFigure:WCDMa:SIGNaling<Instance>:SMS:OUTGoing:UDHeader
	single: CONFigure:WCDMa:SIGNaling<Instance>:SMS:OUTGoing:DCODing
	single: CONFigure:WCDMa:SIGNaling<Instance>:SMS:OUTGoing:CGRoup
	single: CONFigure:WCDMa:SIGNaling<Instance>:SMS:OUTGoing:MCLass
	single: CONFigure:WCDMa:SIGNaling<Instance>:SMS:OUTGoing:OSADdress
	single: CONFigure:WCDMa:SIGNaling<Instance>:SMS:OUTGoing:OADDress
	single: CONFigure:WCDMa:SIGNaling<Instance>:SMS:OUTGoing:RMCDelay
	single: CONFigure:WCDMa:SIGNaling<Instance>:SMS:OUTGoing:LHANdling
	single: CONFigure:WCDMa:SIGNaling<Instance>:SMS:OUTGoing:MESHandling
	single: CONFigure:WCDMa:SIGNaling<Instance>:SMS:OUTGoing:INTernal

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:SMS:OUTGoing:BINary
	CONFigure:WCDMa:SIGNaling<Instance>:SMS:OUTGoing:PIDentifier
	CONFigure:WCDMa:SIGNaling<Instance>:SMS:OUTGoing:UDHeader
	CONFigure:WCDMa:SIGNaling<Instance>:SMS:OUTGoing:DCODing
	CONFigure:WCDMa:SIGNaling<Instance>:SMS:OUTGoing:CGRoup
	CONFigure:WCDMa:SIGNaling<Instance>:SMS:OUTGoing:MCLass
	CONFigure:WCDMa:SIGNaling<Instance>:SMS:OUTGoing:OSADdress
	CONFigure:WCDMa:SIGNaling<Instance>:SMS:OUTGoing:OADDress
	CONFigure:WCDMa:SIGNaling<Instance>:SMS:OUTGoing:RMCDelay
	CONFigure:WCDMa:SIGNaling<Instance>:SMS:OUTGoing:LHANdling
	CONFigure:WCDMa:SIGNaling<Instance>:SMS:OUTGoing:MESHandling
	CONFigure:WCDMa:SIGNaling<Instance>:SMS:OUTGoing:INTernal



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Sms_.Outgoing.Outgoing
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.sms.outgoing.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Sms_Outgoing_SctStamp.rst
	Configure_Sms_Outgoing_File.rst