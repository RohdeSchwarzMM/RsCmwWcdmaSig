Scenario
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:WCDMa:SIGNaling<Instance>:SCENario

.. code-block:: python

	ROUTe:WCDMa:SIGNaling<Instance>:SCENario



.. autoclass:: RsCmwWcdmaSig.Implementations.Route_.Scenario.Scenario
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.scenario.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Scenario_Scell.rst
	Route_Scenario_Dcarrier.rst
	Route_Scenario_ScFading.rst
	Route_Scenario_ScfDiversity.rst
	Route_Scenario_DcFading.rst
	Route_Scenario_DcfDiversity.rst
	Route_Scenario_DbFading.rst
	Route_Scenario_DbfDiversity.rst
	Route_Scenario_Dchspa.rst
	Route_Scenario_Tchspa.rst