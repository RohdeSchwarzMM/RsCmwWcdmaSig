DbFading
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:WCDMa:SIGNaling<Instance>:SCENario:DBFading:EXTernal
	single: ROUTe:WCDMa:SIGNaling<Instance>:SCENario:DBFading:INTernal

.. code-block:: python

	ROUTe:WCDMa:SIGNaling<Instance>:SCENario:DBFading:EXTernal
	ROUTe:WCDMa:SIGNaling<Instance>:SCENario:DBFading:INTernal



.. autoclass:: RsCmwWcdmaSig.Implementations.Route_.Scenario_.DbFading.DbFading
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.scenario.dbFading.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Scenario_DbFading_Flexible.rst