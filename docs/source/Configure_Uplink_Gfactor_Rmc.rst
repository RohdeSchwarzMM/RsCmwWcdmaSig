Rmc<RefMeasChannel>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Ch1 .. Ch5
	rc = driver.configure.uplink.gfactor.rmc.repcap_refMeasChannel_get()
	driver.configure.uplink.gfactor.rmc.repcap_refMeasChannel_set(repcap.RefMeasChannel.Ch1)



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:GFACtor:RMC<RefMeasChannel>

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:UL:GFACtor:RMC<RefMeasChannel>



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Uplink_.Gfactor_.Rmc.Rmc
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.uplink.gfactor.rmc.clone()