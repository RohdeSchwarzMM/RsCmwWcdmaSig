Lsequence
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:ENHanced:DPCH:LSEQuence:STATe
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:ENHanced:DPCH:LSEQuence

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:DL:ENHanced:DPCH:LSEQuence:STATe
	CONFigure:WCDMa:SIGNaling<Instance>:DL:ENHanced:DPCH:LSEQuence



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Downlink_.Enhanced_.Dpch_.Lsequence.Lsequence
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.downlink.enhanced.dpch.lsequence.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Downlink_Enhanced_Dpch_Lsequence_Execute.rst