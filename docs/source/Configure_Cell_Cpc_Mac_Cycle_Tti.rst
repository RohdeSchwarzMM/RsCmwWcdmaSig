Tti<TransTimeInterval>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Tti2 .. Tti10
	rc = driver.configure.cell.cpc.mac.cycle.tti.repcap_transTimeInterval_get()
	driver.configure.cell.cpc.mac.cycle.tti.repcap_transTimeInterval_set(repcap.TransTimeInterval.Tti2)



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:MAC:CYCLe:TTI<TransTimeInterval>

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:MAC:CYCLe:TTI<TransTimeInterval>



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Cpc_.Mac_.Cycle_.Tti.Tti
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.cpc.mac.cycle.tti.clone()