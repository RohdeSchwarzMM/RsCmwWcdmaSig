Connection
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:SRBData
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:UETerminate
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:CID

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:SRBData
	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:UETerminate
	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:CID



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Connection.Connection
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Voice.rst
	Configure_Connection_Tmode.rst
	Configure_Connection_Packet.rst
	Configure_Connection_SrbSingle.rst
	Configure_Connection_Video.rst
	Configure_Connection_Cswitched.rst