Flexible
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:WCDMa:SIGNaling<Instance>:SCENario:DCFading:FLEXible:EXTernal
	single: ROUTe:WCDMa:SIGNaling<Instance>:SCENario:DCFading:FLEXible:INTernal

.. code-block:: python

	ROUTe:WCDMa:SIGNaling<Instance>:SCENario:DCFading:FLEXible:EXTernal
	ROUTe:WCDMa:SIGNaling<Instance>:SCENario:DCFading:FLEXible:INTernal



.. autoclass:: RsCmwWcdmaSig.Implementations.Route_.Scenario_.DcFading_.Flexible.Flexible
	:members:
	:undoc-members:
	:noindex: