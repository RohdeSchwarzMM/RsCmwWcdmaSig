Etws
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CBS:MESSage:ETWS:ALERt
	single: CONFigure:WCDMa:SIGNaling<Instance>:CBS:MESSage:ETWS:POPup

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CBS:MESSage:ETWS:ALERt
	CONFigure:WCDMa:SIGNaling<Instance>:CBS:MESSage:ETWS:POPup



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cbs_.Message_.Etws.Etws
	:members:
	:undoc-members:
	:noindex: