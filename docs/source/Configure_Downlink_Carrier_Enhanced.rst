Enhanced
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Downlink_.Carrier_.Enhanced.Enhanced
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.downlink.carrier.enhanced.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Downlink_Carrier_Enhanced_Dpch.rst
	Configure_Downlink_Carrier_Enhanced_Pcpich.rst
	Configure_Downlink_Carrier_Enhanced_HsPdsch.rst
	Configure_Downlink_Carrier_Enhanced_Hsscch.rst