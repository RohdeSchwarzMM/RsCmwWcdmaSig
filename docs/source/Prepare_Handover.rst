Handover
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: PREPare:WCDMa:SIGNaling<Instance>:HANDover:DESTination
	single: PREPare:WCDMa:SIGNaling<Instance>:HANDover:MMODe

.. code-block:: python

	PREPare:WCDMa:SIGNaling<Instance>:HANDover:DESTination
	PREPare:WCDMa:SIGNaling<Instance>:HANDover:MMODe



.. autoclass:: RsCmwWcdmaSig.Implementations.Prepare_.Handover.Handover
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.prepare.handover.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Prepare_Handover_Catalog.rst
	Prepare_Handover_External.rst