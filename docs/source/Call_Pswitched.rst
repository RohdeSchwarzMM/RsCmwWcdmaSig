Pswitched
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALL:WCDMa:SIGNaling<Instance>:PSWitched:ACTion

.. code-block:: python

	CALL:WCDMa:SIGNaling<Instance>:PSWitched:ACTion



.. autoclass:: RsCmwWcdmaSig.Implementations.Call_.Pswitched.Pswitched
	:members:
	:undoc-members:
	:noindex: