Cell
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:WCDMa:SIGNaling<Instance>:UEReport:NCELl:GSM:CELL<Cell>

.. code-block:: python

	SENSe:WCDMa:SIGNaling<Instance>:UEReport:NCELl:GSM:CELL<Cell>



.. autoclass:: RsCmwWcdmaSig.Implementations.Sense_.UeReport_.Ncell_.Gsm_.Cell.Cell
	:members:
	:undoc-members:
	:noindex: