Trace
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Throughput_.Trace.Trace
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.throughput.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Throughput_Trace_Downlink.rst
	Throughput_Trace_Uplink.rst