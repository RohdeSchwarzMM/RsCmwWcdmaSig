Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:THRoughput:TRACe:DL:PDU:AVERage
	single: READ:WCDMa:SIGNaling<Instance>:THRoughput:TRACe:DL:PDU:AVERage

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:THRoughput:TRACe:DL:PDU:AVERage
	READ:WCDMa:SIGNaling<Instance>:THRoughput:TRACe:DL:PDU:AVERage



.. autoclass:: RsCmwWcdmaSig.Implementations.Throughput_.Trace_.Downlink_.Pdu_.Average.Average
	:members:
	:undoc-members:
	:noindex: