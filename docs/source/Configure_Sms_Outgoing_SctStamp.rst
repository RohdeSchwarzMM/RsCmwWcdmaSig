SctStamp
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:SMS:OUTGoing:SCTStamp:TSOurce
	single: CONFigure:WCDMa:SIGNaling<Instance>:SMS:OUTGoing:SCTStamp:DATE
	single: CONFigure:WCDMa:SIGNaling<Instance>:SMS:OUTGoing:SCTStamp:TIME

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:SMS:OUTGoing:SCTStamp:TSOurce
	CONFigure:WCDMa:SIGNaling<Instance>:SMS:OUTGoing:SCTStamp:DATE
	CONFigure:WCDMa:SIGNaling<Instance>:SMS:OUTGoing:SCTStamp:TIME



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Sms_.Outgoing_.SctStamp.SctStamp
	:members:
	:undoc-members:
	:noindex: