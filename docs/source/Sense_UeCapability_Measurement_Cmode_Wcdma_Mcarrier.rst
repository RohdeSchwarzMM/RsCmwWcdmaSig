Mcarrier
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:WCDMa:SIGNaling<Instance>:UECapability:MEASurement:CMODe:WCDMa:MCARrier

.. code-block:: python

	SENSe:WCDMa:SIGNaling<Instance>:UECapability:MEASurement:CMODe:WCDMa:MCARrier



.. autoclass:: RsCmwWcdmaSig.Implementations.Sense_.UeCapability_.Measurement_.Cmode_.Wcdma_.Mcarrier.Mcarrier
	:members:
	:undoc-members:
	:noindex: