Cell
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:NCELl:WCDMa:CELL<Cell>

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:NCELl:WCDMa:CELL<Cell>



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Ncell_.Wcdma_.Cell.Cell
	:members:
	:undoc-members:
	:noindex: