Psettings
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:PSETtings:ERGM
	single: CONFigure:WCDMa:SIGNaling<Instance>:PSETtings:HUMP
	single: CONFigure:WCDMa:SIGNaling<Instance>:PSETtings

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:PSETtings:ERGM
	CONFigure:WCDMa:SIGNaling<Instance>:PSETtings:HUMP
	CONFigure:WCDMa:SIGNaling<Instance>:PSETtings



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Psettings.Psettings
	:members:
	:undoc-members:
	:noindex: