UserDefined
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSDPa:UDEFined:HARQ
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSDPa:UDEFined:IRBuffer

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSDPa:UDEFined:HARQ
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSDPa:UDEFined:IRBuffer



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Hsdpa_.UserDefined.UserDefined
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.hsdpa.userDefined.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cell_Hsdpa_UserDefined_RvcSequences.rst