Downlink
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:UDEFined:FREQuency:DL:MINimum
	single: CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:UDEFined:FREQuency:DL:MAXimum

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:UDEFined:FREQuency:DL:MINimum
	CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:UDEFined:FREQuency:DL:MAXimum



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.RfSettings_.UserDefined_.Frequency_.Downlink.Downlink
	:members:
	:undoc-members:
	:noindex: