UesInfo
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:WCDMa:SIGNaling<Instance>:UESinfo:APN
	single: SENSe:WCDMa:SIGNaling<Instance>:UESinfo:DULalignment
	single: SENSe:WCDMa:SIGNaling<Instance>:UESinfo:DINFo
	single: SENSe:WCDMa:SIGNaling<Instance>:UESinfo:IMEI
	single: SENSe:WCDMa:SIGNaling<Instance>:UESinfo:RIDentity
	single: SENSe:WCDMa:SIGNaling<Instance>:UESinfo:RITYpe
	single: SENSe:WCDMa:SIGNaling<Instance>:UESinfo:TTY
	single: SENSe:WCDMa:SIGNaling<Instance>:UESinfo:CNUMber
	single: SENSe:WCDMa:SIGNaling<Instance>:UESinfo:DNUMber
	single: SENSe:WCDMa:SIGNaling<Instance>:UESinfo:EMERgency
	single: SENSe:WCDMa:SIGNaling<Instance>:UESinfo:ESCategory
	single: SENSe:WCDMa:SIGNaling<Instance>:UESinfo:RRC

.. code-block:: python

	SENSe:WCDMa:SIGNaling<Instance>:UESinfo:APN
	SENSe:WCDMa:SIGNaling<Instance>:UESinfo:DULalignment
	SENSe:WCDMa:SIGNaling<Instance>:UESinfo:DINFo
	SENSe:WCDMa:SIGNaling<Instance>:UESinfo:IMEI
	SENSe:WCDMa:SIGNaling<Instance>:UESinfo:RIDentity
	SENSe:WCDMa:SIGNaling<Instance>:UESinfo:RITYpe
	SENSe:WCDMa:SIGNaling<Instance>:UESinfo:TTY
	SENSe:WCDMa:SIGNaling<Instance>:UESinfo:CNUMber
	SENSe:WCDMa:SIGNaling<Instance>:UESinfo:DNUMber
	SENSe:WCDMa:SIGNaling<Instance>:UESinfo:EMERgency
	SENSe:WCDMa:SIGNaling<Instance>:UESinfo:ESCategory
	SENSe:WCDMa:SIGNaling<Instance>:UESinfo:RRC



.. autoclass:: RsCmwWcdmaSig.Implementations.Sense_.UesInfo.UesInfo
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.uesInfo.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UesInfo_UeAddress.rst
	Sense_UesInfo_Connection.rst