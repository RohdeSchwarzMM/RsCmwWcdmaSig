Horder
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:CARRier<Carrier>:HORDer:DL
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:CARRier<Carrier>:HORDer:UL

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:CARRier<Carrier>:HORDer:DL
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:CARRier<Carrier>:HORDer:UL



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Carrier_.Horder.Horder
	:members:
	:undoc-members:
	:noindex: