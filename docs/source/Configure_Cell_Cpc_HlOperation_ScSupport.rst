ScSupport<SecondCode>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Sc1 .. Sc4
	rc = driver.configure.cell.cpc.hlOperation.scSupport.repcap_secondCode_get()
	driver.configure.cell.cpc.hlOperation.scSupport.repcap_secondCode_set(repcap.SecondCode.Sc1)



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:HLOPeration:SCSupport<SecondCode>

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:HLOPeration:SCSupport<SecondCode>



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Cpc_.HlOperation_.ScSupport.ScSupport
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.cpc.hlOperation.scSupport.clone()