Downlink
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Downlink.Downlink
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.downlink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Downlink_Carrier.rst
	Configure_Downlink_Level.rst
	Configure_Downlink_Code.rst
	Configure_Downlink_Enhanced.rst
	Configure_Downlink_Pcontrol.rst