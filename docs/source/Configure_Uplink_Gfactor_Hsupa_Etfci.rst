Etfci
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:GFACtor:HSUPa:ETFCi:POFFset
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:GFACtor:HSUPa:ETFCi:REFerence
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:GFACtor:HSUPa:ETFCi:NUMBer
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:GFACtor:HSUPa:ETFCi:BOOSt

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:UL:GFACtor:HSUPa:ETFCi:POFFset
	CONFigure:WCDMa:SIGNaling<Instance>:UL:GFACtor:HSUPa:ETFCi:REFerence
	CONFigure:WCDMa:SIGNaling<Instance>:UL:GFACtor:HSUPa:ETFCi:NUMBer
	CONFigure:WCDMa:SIGNaling<Instance>:UL:GFACtor:HSUPa:ETFCi:BOOSt



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Uplink_.Gfactor_.Hsupa_.Etfci.Etfci
	:members:
	:undoc-members:
	:noindex: