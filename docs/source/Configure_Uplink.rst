Uplink
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:MUEPower

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:UL:MUEPower



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Uplink.Uplink
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.uplink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Uplink_UepClass.rst
	Configure_Uplink_Carrier.rst
	Configure_Uplink_OlpControl.rst
	Configure_Uplink_Prach.rst
	Configure_Uplink_Gfactor.rst
	Configure_Uplink_Tpc.rst
	Configure_Uplink_Tpcset.rst