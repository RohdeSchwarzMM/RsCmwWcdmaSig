Cell
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:MRVersion
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:RSIGnaling
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:PSDomain
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:IDENtity
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:IDNode
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:RNC
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:URA
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:RAC
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:LAC
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:NTOPeration
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:MCC
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:BINDicator

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:MRVersion
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:RSIGnaling
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:PSDomain
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:IDENtity
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:IDNode
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:RNC
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:URA
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:RAC
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:LAC
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:NTOPeration
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:MCC
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:BINDicator



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell.Cell
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cell_Carrier.rst
	Configure_Cell_Rcause.rst
	Configure_Cell_Timeout.rst
	Configure_Cell_Request.rst
	Configure_Cell_Security.rst
	Configure_Cell_UeIdentity.rst
	Configure_Cell_Mnc.rst
	Configure_Cell_ReSelection.rst
	Configure_Cell_Time.rst
	Configure_Cell_Sync.rst
	Configure_Cell_Hsdpa.rst
	Configure_Cell_Hsupa.rst
	Configure_Cell_Horder.rst
	Configure_Cell_Cpc.rst