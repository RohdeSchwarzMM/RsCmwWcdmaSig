Dsg
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:UDTX:CYCLe<Cycle>:DSG

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:UDTX:CYCLe<Cycle>:DSG



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Cpc_.Udtx_.Cycle_.Dsg.Dsg
	:members:
	:undoc-members:
	:noindex: