Etfci
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSUPa:ETFCi:TINDex

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSUPa:ETFCi:TINDex



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Hsupa_.Etfci.Etfci
	:members:
	:undoc-members:
	:noindex: