Fsimulator
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:FADing:CARRier<Carrier>:FSIMulator:ENABle
	single: CONFigure:WCDMa:SIGNaling<Instance>:FADing:CARRier<Carrier>:FSIMulator:STANdard

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:FADing:CARRier<Carrier>:FSIMulator:ENABle
	CONFigure:WCDMa:SIGNaling<Instance>:FADing:CARRier<Carrier>:FSIMulator:STANdard



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Fading_.Carrier_.Fsimulator.Fsimulator
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.fading.carrier.fsimulator.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Fading_Carrier_Fsimulator_Globale.rst
	Configure_Fading_Carrier_Fsimulator_Restart.rst
	Configure_Fading_Carrier_Fsimulator_Iloss.rst
	Configure_Fading_Carrier_Fsimulator_Dshift.rst