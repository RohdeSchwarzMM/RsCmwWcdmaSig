Harq
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSUPa:HARQ:POFFset
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSUPa:HARQ:RETX

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSUPa:HARQ:POFFset
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSUPa:HARQ:RETX



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Hsupa_.Harq.Harq
	:members:
	:undoc-members:
	:noindex: