Sense
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Sense.Sense
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Elogging.rst
	Sense_UeReport.rst
	Sense_UeCapability.rst
	Sense_UesInfo.rst
	Sense_Cell.rst
	Sense_IqOut.rst
	Sense_Downlink.rst
	Sense_Uplink.rst
	Sense_Connection.rst
	Sense_Sms.rst
	Sense_Fading.rst