Network
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:PACKet:INACtivity:DCH:NETWork:ENABle
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:PACKet:INACtivity:DCH:NETWork:TIMer
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:PACKet:INACtivity:DCH:NETWork:DSTate

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:PACKet:INACtivity:DCH:NETWork:ENABle
	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:PACKet:INACtivity:DCH:NETWork:TIMer
	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:PACKet:INACtivity:DCH:NETWork:DSTate



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Connection_.Packet_.Inactivity_.Dch_.Network.Network
	:members:
	:undoc-members:
	:noindex: