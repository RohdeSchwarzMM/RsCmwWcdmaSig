Ehrch
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:CARRier<Carrier>:HSUPa:EHRCh:FUFDummies

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:CARRier<Carrier>:HSUPa:EHRCh:FUFDummies



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Carrier_.Hsupa_.Ehrch.Ehrch
	:members:
	:undoc-members:
	:noindex: