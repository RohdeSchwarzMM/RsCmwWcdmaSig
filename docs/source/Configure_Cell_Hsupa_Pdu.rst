Pdu
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSUPa:PDU:FLEXible
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSUPa:PDU

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSUPa:PDU:FLEXible
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSUPa:PDU



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Hsupa_.Pdu.Pdu
	:members:
	:undoc-members:
	:noindex: