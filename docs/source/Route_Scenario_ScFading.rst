ScFading
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:WCDMa:SIGNaling<Instance>:SCENario:SCFading:EXTernal
	single: ROUTe:WCDMa:SIGNaling<Instance>:SCENario:SCFading:INTernal

.. code-block:: python

	ROUTe:WCDMa:SIGNaling<Instance>:SCENario:SCFading:EXTernal
	ROUTe:WCDMa:SIGNaling<Instance>:SCENario:SCFading:INTernal



.. autoclass:: RsCmwWcdmaSig.Implementations.Route_.Scenario_.ScFading.ScFading
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.scenario.scFading.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Scenario_ScFading_Flexible.rst