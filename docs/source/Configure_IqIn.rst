IqIn
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:IQIN:CARRier<Carrier>

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:IQIN:CARRier<Carrier>



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.IqIn.IqIn
	:members:
	:undoc-members:
	:noindex: