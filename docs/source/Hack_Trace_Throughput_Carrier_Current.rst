Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:HACK:TRACe:THRoughput:CARRier<Carrier>:CURRent
	single: READ:WCDMa:SIGNaling<Instance>:HACK:TRACe:THRoughput:CARRier<Carrier>:CURRent

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:HACK:TRACe:THRoughput:CARRier<Carrier>:CURRent
	READ:WCDMa:SIGNaling<Instance>:HACK:TRACe:THRoughput:CARRier<Carrier>:CURRent



.. autoclass:: RsCmwWcdmaSig.Implementations.Hack_.Trace_.Throughput_.Carrier_.Current.Current
	:members:
	:undoc-members:
	:noindex: