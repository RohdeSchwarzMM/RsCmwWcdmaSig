Trace
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Eagch_.Trace.Trace
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.eagch.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Eagch_Trace_General.rst