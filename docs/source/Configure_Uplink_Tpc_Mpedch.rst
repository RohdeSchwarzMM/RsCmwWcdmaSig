Mpedch
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:TPC:MPEDch:STATe

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:UL:TPC:MPEDch:STATe



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Uplink_.Tpc_.Mpedch.Mpedch
	:members:
	:undoc-members:
	:noindex: