State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:BER:STATe

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:BER:STATe



.. autoclass:: RsCmwWcdmaSig.Implementations.Ber_.State.State
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ber.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Ber_State_All.rst