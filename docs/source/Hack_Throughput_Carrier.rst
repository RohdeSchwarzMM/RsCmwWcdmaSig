Carrier
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Hack_.Throughput_.Carrier.Carrier
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.hack.throughput.carrier.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Hack_Throughput_Carrier_Relative.rst
	Hack_Throughput_Carrier_Absolute.rst