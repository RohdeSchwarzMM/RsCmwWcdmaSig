Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:WCDMa:SIGNaling<Instance>:HACK:TRACe:SUBFrame:CARRier<Carrier>:TBLock:MINimum
	single: FETCh:WCDMa:SIGNaling<Instance>:HACK:TRACe:SUBFrame:CARRier<Carrier>:TBLock:MINimum

.. code-block:: python

	READ:WCDMa:SIGNaling<Instance>:HACK:TRACe:SUBFrame:CARRier<Carrier>:TBLock:MINimum
	FETCh:WCDMa:SIGNaling<Instance>:HACK:TRACe:SUBFrame:CARRier<Carrier>:TBLock:MINimum



.. autoclass:: RsCmwWcdmaSig.Implementations.Hack_.Trace_.Subframe_.Carrier_.Tblock_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: