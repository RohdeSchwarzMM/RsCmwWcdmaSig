Throughput
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:THRoughput:TOUT
	single: CONFigure:WCDMa:SIGNaling<Instance>:THRoughput:UPDate
	single: CONFigure:WCDMa:SIGNaling<Instance>:THRoughput:WINDow
	single: CONFigure:WCDMa:SIGNaling<Instance>:THRoughput:REPetition

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:THRoughput:TOUT
	CONFigure:WCDMa:SIGNaling<Instance>:THRoughput:UPDate
	CONFigure:WCDMa:SIGNaling<Instance>:THRoughput:WINDow
	CONFigure:WCDMa:SIGNaling<Instance>:THRoughput:REPetition



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Throughput.Throughput
	:members:
	:undoc-members:
	:noindex: