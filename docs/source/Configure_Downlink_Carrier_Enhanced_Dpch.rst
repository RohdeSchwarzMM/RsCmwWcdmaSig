Dpch
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:ENHanced:DPCH:FSFormat

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:ENHanced:DPCH:FSFormat



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Downlink_.Carrier_.Enhanced_.Dpch.Dpch
	:members:
	:undoc-members:
	:noindex: