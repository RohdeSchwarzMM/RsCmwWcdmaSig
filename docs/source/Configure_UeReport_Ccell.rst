Ccell
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:UEReport:CCELl:ENABle

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:UEReport:CCELl:ENABle



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.UeReport_.Ccell.Ccell
	:members:
	:undoc-members:
	:noindex: