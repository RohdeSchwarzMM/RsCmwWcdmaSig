Lte
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Sense_.UeReport_.Ncell_.Lte.Lte
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ueReport.ncell.lte.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UeReport_Ncell_Lte_Cell.rst