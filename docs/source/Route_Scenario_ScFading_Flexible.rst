Flexible
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:WCDMa:SIGNaling<Instance>:SCENario:SCFading:FLEXible:EXTernal
	single: ROUTe:WCDMa:SIGNaling<Instance>:SCENario:SCFading:FLEXible:INTernal

.. code-block:: python

	ROUTe:WCDMa:SIGNaling<Instance>:SCENario:SCFading:FLEXible:EXTernal
	ROUTe:WCDMa:SIGNaling<Instance>:SCENario:SCFading:FLEXible:INTernal



.. autoclass:: RsCmwWcdmaSig.Implementations.Route_.Scenario_.ScFading_.Flexible.Flexible
	:members:
	:undoc-members:
	:noindex: