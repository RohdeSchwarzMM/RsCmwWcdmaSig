Btfd
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:TMODe:BTFD:TFORmat

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:TMODe:BTFD:TFORmat



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Connection_.Tmode_.Btfd.Btfd
	:members:
	:undoc-members:
	:noindex: