Rohc
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:PACKet:ROHC:ENABle
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:PACKet:ROHC:PROFiles

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:PACKet:ROHC:ENABle
	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:PACKet:ROHC:PROFiles



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Connection_.Packet_.Rohc.Rohc
	:members:
	:undoc-members:
	:noindex: