Carrier
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Fading_.Carrier.Carrier
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.fading.carrier.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Fading_Carrier_Fsimulator.rst
	Configure_Fading_Carrier_Awgn.rst
	Configure_Fading_Carrier_Power.rst