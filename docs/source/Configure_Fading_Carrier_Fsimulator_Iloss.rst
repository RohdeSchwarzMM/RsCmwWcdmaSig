Iloss
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:FADing:CARRier<Carrier>:FSIMulator:ILOSs:MODE
	single: CONFigure:WCDMa:SIGNaling<Instance>:FADing:CARRier<Carrier>:FSIMulator:ILOSs:LOSS

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:FADing:CARRier<Carrier>:FSIMulator:ILOSs:MODE
	CONFigure:WCDMa:SIGNaling<Instance>:FADing:CARRier<Carrier>:FSIMulator:ILOSs:LOSS



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Fading_.Carrier_.Fsimulator_.Iloss.Iloss
	:members:
	:undoc-members:
	:noindex: