Reject
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CLEan:WCDMa:SIGNaling<Instance>:CONNection:CSWitched:REJect

.. code-block:: python

	CLEan:WCDMa:SIGNaling<Instance>:CONNection:CSWitched:REJect



.. autoclass:: RsCmwWcdmaSig.Implementations.Clean_.Connection_.Cswitched_.Reject.Reject
	:members:
	:undoc-members:
	:noindex: