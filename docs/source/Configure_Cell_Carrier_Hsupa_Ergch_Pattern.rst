Pattern
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:CARRier<Carrier>:HSUPa:ERGCh:PATTern:LENGth
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:CARRier<Carrier>:HSUPa:ERGCh:PATTern

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:CARRier<Carrier>:HSUPa:ERGCh:PATTern:LENGth
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:CARRier<Carrier>:HSUPa:ERGCh:PATTern



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Carrier_.Hsupa_.Ergch_.Pattern.Pattern
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.carrier.hsupa.ergch.pattern.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cell_Carrier_Hsupa_Ergch_Pattern_Execute.rst