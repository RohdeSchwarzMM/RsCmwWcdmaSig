Catalog
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: PREPare:WCDMa:SIGNaling<Instance>:HANDover:CATalog:DESTination

.. code-block:: python

	PREPare:WCDMa:SIGNaling<Instance>:HANDover:CATalog:DESTination



.. autoclass:: RsCmwWcdmaSig.Implementations.Prepare_.Handover_.Catalog.Catalog
	:members:
	:undoc-members:
	:noindex: