Cell
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:WCDMa:SIGNaling<Instance>:UEReport:NCELl:WCDMa:CELL<Cell>

.. code-block:: python

	SENSe:WCDMa:SIGNaling<Instance>:UEReport:NCELl:WCDMa:CELL<Cell>



.. autoclass:: RsCmwWcdmaSig.Implementations.Sense_.UeReport_.Ncell_.Wcdma_.Cell.Cell
	:members:
	:undoc-members:
	:noindex: