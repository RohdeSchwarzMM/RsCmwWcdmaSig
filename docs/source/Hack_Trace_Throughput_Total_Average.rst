Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:HACK:TRACe:THRoughput:TOTal:AVERage
	single: READ:WCDMa:SIGNaling<Instance>:HACK:TRACe:THRoughput:TOTal:AVERage

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:HACK:TRACe:THRoughput:TOTal:AVERage
	READ:WCDMa:SIGNaling<Instance>:HACK:TRACe:THRoughput:TOTal:AVERage



.. autoclass:: RsCmwWcdmaSig.Implementations.Hack_.Trace_.Throughput_.Total_.Average.Average
	:members:
	:undoc-members:
	:noindex: