Ber
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:BER:PNResync
	single: CONFigure:WCDMa:SIGNaling<Instance>:BER:LIMit
	single: CONFigure:WCDMa:SIGNaling<Instance>:BER:TBLocks
	single: CONFigure:WCDMa:SIGNaling<Instance>:BER:SCONdition
	single: CONFigure:WCDMa:SIGNaling<Instance>:BER:REPetition
	single: CONFigure:WCDMa:SIGNaling<Instance>:BER:TOUT

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:BER:PNResync
	CONFigure:WCDMa:SIGNaling<Instance>:BER:LIMit
	CONFigure:WCDMa:SIGNaling<Instance>:BER:TBLocks
	CONFigure:WCDMa:SIGNaling<Instance>:BER:SCONdition
	CONFigure:WCDMa:SIGNaling<Instance>:BER:REPetition
	CONFigure:WCDMa:SIGNaling<Instance>:BER:TOUT



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Ber.Ber
	:members:
	:undoc-members:
	:noindex: