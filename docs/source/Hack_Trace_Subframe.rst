Subframe
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Hack_.Trace_.Subframe.Subframe
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.hack.trace.subframe.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Hack_Trace_Subframe_Carrier.rst