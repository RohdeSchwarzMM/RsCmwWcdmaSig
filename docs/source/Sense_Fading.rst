Fading
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Sense_.Fading.Fading
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.fading.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Fading_Carrier.rst