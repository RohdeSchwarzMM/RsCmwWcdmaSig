Throughput
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Hack_.Throughput.Throughput
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.hack.throughput.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Hack_Throughput_Carrier.rst