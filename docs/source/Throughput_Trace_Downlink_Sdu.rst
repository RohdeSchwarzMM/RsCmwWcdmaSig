Sdu
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Throughput_.Trace_.Downlink_.Sdu.Sdu
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.throughput.trace.downlink.sdu.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Throughput_Trace_Downlink_Sdu_Current.rst
	Throughput_Trace_Downlink_Sdu_Average.rst