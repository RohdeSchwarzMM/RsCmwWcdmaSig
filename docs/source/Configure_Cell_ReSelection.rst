ReSelection
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:RESelection:SEARch
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:RESelection:QUALity
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:RESelection:TIME

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:RESelection:SEARch
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:RESelection:QUALity
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:RESelection:TIME



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.ReSelection.ReSelection
	:members:
	:undoc-members:
	:noindex: