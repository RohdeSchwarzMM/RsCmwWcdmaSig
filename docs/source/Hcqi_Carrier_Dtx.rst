Dtx
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:HCQI:CARRier<Carrier>:DTX
	single: READ:WCDMa:SIGNaling<Instance>:HCQI:CARRier<Carrier>:DTX

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:HCQI:CARRier<Carrier>:DTX
	READ:WCDMa:SIGNaling<Instance>:HCQI:CARRier<Carrier>:DTX



.. autoclass:: RsCmwWcdmaSig.Implementations.Hcqi_.Carrier_.Dtx.Dtx
	:members:
	:undoc-members:
	:noindex: