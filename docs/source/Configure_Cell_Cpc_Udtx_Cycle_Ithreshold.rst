Ithreshold
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:UDTX:CYCLe<Cycle>:ITHReshold

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:UDTX:CYCLe<Cycle>:ITHReshold



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Cpc_.Udtx_.Cycle_.Ithreshold.Ithreshold
	:members:
	:undoc-members:
	:noindex: