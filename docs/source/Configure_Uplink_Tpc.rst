Tpc
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:TPC:STATe
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:TPC:PATTern
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:TPC:MODE

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:UL:TPC:STATe
	CONFigure:WCDMa:SIGNaling<Instance>:UL:TPC:PATTern
	CONFigure:WCDMa:SIGNaling<Instance>:UL:TPC:MODE



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Uplink_.Tpc.Tpc
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.uplink.tpc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Uplink_Tpc_Set.rst
	Configure_Uplink_Tpc_Tpower.rst
	Configure_Uplink_Tpc_Mpedch.rst
	Configure_Uplink_Tpc_Precondition.rst
	Configure_Uplink_Tpc_Pexecute.rst