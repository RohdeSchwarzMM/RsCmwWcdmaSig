State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:HCQI:STATe

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:HCQI:STATe



.. autoclass:: RsCmwWcdmaSig.Implementations.Hcqi_.State.State
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.hcqi.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Hcqi_State_All.rst