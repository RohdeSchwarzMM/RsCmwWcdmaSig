Tpcset
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Uplink_.Tpcset.Tpcset
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.uplink.tpcset.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Uplink_Tpcset_Pconfig.rst
	Configure_Uplink_Tpcset_Precondition.rst