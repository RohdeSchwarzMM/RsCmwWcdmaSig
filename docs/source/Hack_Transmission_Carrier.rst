Carrier
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:HACK:TRANsmission:CARRier<Carrier>
	single: READ:WCDMa:SIGNaling<Instance>:HACK:TRANsmission:CARRier<Carrier>

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:HACK:TRANsmission:CARRier<Carrier>
	READ:WCDMa:SIGNaling<Instance>:HACK:TRANsmission:CARRier<Carrier>



.. autoclass:: RsCmwWcdmaSig.Implementations.Hack_.Transmission_.Carrier.Carrier
	:members:
	:undoc-members:
	:noindex: