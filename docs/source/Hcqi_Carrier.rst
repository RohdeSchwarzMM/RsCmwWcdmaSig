Carrier
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:WCDMa:SIGNaling<Instance>:HCQI:CARRier<Carrier>
	single: READ:WCDMa:SIGNaling<Instance>:HCQI:CARRier<Carrier>

.. code-block:: python

	FETCh:WCDMa:SIGNaling<Instance>:HCQI:CARRier<Carrier>
	READ:WCDMa:SIGNaling<Instance>:HCQI:CARRier<Carrier>



.. autoclass:: RsCmwWcdmaSig.Implementations.Hcqi_.Carrier.Carrier
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.hcqi.carrier.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Hcqi_Carrier_Bler.rst
	Hcqi_Carrier_Dtx.rst
	Hcqi_Carrier_MsFrames.rst