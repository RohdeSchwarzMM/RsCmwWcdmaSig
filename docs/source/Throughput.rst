Throughput
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STOP:WCDMa:SIGNaling<Instance>:THRoughput
	single: ABORt:WCDMa:SIGNaling<Instance>:THRoughput
	single: INITiate:WCDMa:SIGNaling<Instance>:THRoughput
	single: FETCh:WCDMa:SIGNaling<Instance>:THRoughput
	single: READ:WCDMa:SIGNaling<Instance>:THRoughput

.. code-block:: python

	STOP:WCDMa:SIGNaling<Instance>:THRoughput
	ABORt:WCDMa:SIGNaling<Instance>:THRoughput
	INITiate:WCDMa:SIGNaling<Instance>:THRoughput
	FETCh:WCDMa:SIGNaling<Instance>:THRoughput
	READ:WCDMa:SIGNaling<Instance>:THRoughput



.. autoclass:: RsCmwWcdmaSig.Implementations.Throughput.Throughput
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.throughput.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Throughput_State.rst
	Throughput_Trace.rst