RfParameter
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:WCDMa:SIGNaling<Instance>:UECapability:RFParameter:BCList
	single: SENSe:WCDMa:SIGNaling<Instance>:UECapability:RFParameter

.. code-block:: python

	SENSe:WCDMa:SIGNaling<Instance>:UECapability:RFParameter:BCList
	SENSe:WCDMa:SIGNaling<Instance>:UECapability:RFParameter



.. autoclass:: RsCmwWcdmaSig.Implementations.Sense_.UeCapability_.RfParameter.RfParameter
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ueCapability.rfParameter.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UeCapability_RfParameter_Band.rst
	Sense_UeCapability_RfParameter_Bc.rst