Eattenuation
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:CARRier<Carrier>:EATTenuation:INPut
	single: CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:CARRier<Carrier>:EATTenuation:OUTPut

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:CARRier<Carrier>:EATTenuation:INPut
	CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:CARRier<Carrier>:EATTenuation:OUTPut



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.RfSettings_.Carrier_.Eattenuation.Eattenuation
	:members:
	:undoc-members:
	:noindex: