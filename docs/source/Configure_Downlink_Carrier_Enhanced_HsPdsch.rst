HsPdsch
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:ENHanced:HSPDsch:USFRames
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:ENHanced:HSPDsch:POFFset

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:ENHanced:HSPDsch:USFRames
	CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:ENHanced:HSPDsch:POFFset



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Downlink_.Carrier_.Enhanced_.HsPdsch.HsPdsch
	:members:
	:undoc-members:
	:noindex: