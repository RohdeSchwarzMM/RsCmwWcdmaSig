Code
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:CODE:CONFlict
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:CODE:HSPDsch
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:CODE:EAGCh
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:CODE:ERGCh
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:CODE:EHICh
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:CODE:PCPich

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:CODE:CONFlict
	CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:CODE:HSPDsch
	CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:CODE:EAGCh
	CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:CODE:ERGCh
	CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:CODE:EHICh
	CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:CODE:PCPich



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Downlink_.Carrier_.Code.Code
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.downlink.carrier.code.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Downlink_Carrier_Code_Hsscch.rst