Nc<NonContigCell>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nc2 .. Nc4
	rc = driver.sense.ueCapability.rfParameter.band.nc.repcap_nonContigCell_get()
	driver.sense.ueCapability.rfParameter.band.nc.repcap_nonContigCell_set(repcap.NonContigCell.Nc2)



.. rubric:: SCPI Commands

.. index::
	single: SENSe:WCDMa:SIGNaling<Instance>:UECapability:RFParameter:BAND<Band>:NC<NonContigCell>

.. code-block:: python

	SENSe:WCDMa:SIGNaling<Instance>:UECapability:RFParameter:BAND<Band>:NC<NonContigCell>



.. autoclass:: RsCmwWcdmaSig.Implementations.Sense_.UeCapability_.RfParameter_.Band_.Nc.Nc
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ueCapability.rfParameter.band.nc.clone()