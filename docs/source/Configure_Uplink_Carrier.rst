Carrier
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:CARRier<Carrier>:POFFset
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:CARRier<Carrier>:SCODe

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:UL:CARRier<Carrier>:POFFset
	CONFigure:WCDMa:SIGNaling<Instance>:UL:CARRier<Carrier>:SCODe



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Uplink_.Carrier.Carrier
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.uplink.carrier.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Uplink_Carrier_Tpc.rst