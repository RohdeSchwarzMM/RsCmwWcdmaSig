RfSettings
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:DBDC
	single: CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:ENPMode
	single: CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:ENPower
	single: CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:MARGin

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:DBDC
	CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:ENPMode
	CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:ENPower
	CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:MARGin



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.RfSettings.RfSettings
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Dcarrier.rst
	Configure_RfSettings_Carrier.rst
	Configure_RfSettings_CoPower.rst
	Configure_RfSettings_ToPower.rst
	Configure_RfSettings_UserDefined.rst