UeReport
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.UeReport.UeReport
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ueReport.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	UeReport_State.rst