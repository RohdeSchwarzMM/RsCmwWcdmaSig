Uplink
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:CARRier<Carrier>:UL

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:RFSettings:CARRier<Carrier>:UL



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.RfSettings_.Carrier_.Uplink.Uplink
	:members:
	:undoc-members:
	:noindex: