Fading
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:HCQI:LIMit:FADing:BLER
	single: CONFigure:WCDMa:SIGNaling<Instance>:HCQI:LIMit:FADing:DTX

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:HCQI:LIMit:FADing:BLER
	CONFigure:WCDMa:SIGNaling<Instance>:HCQI:LIMit:FADing:DTX



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Hcqi_.Limit_.Fading.Fading
	:members:
	:undoc-members:
	:noindex: