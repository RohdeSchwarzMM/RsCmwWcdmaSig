Pattern
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:CARRier<Carrier>:HSUPa:EAGCh:PATTern:LENGth
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:CARRier<Carrier>:HSUPa:EAGCh:PATTern:INDex
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:CARRier<Carrier>:HSUPa:EAGCh:PATTern:SCOPe
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:CARRier<Carrier>:HSUPa:EAGCh:PATTern:TYPE
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:CARRier<Carrier>:HSUPa:EAGCh:PATTern:REPetition

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:CARRier<Carrier>:HSUPa:EAGCh:PATTern:LENGth
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:CARRier<Carrier>:HSUPa:EAGCh:PATTern:INDex
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:CARRier<Carrier>:HSUPa:EAGCh:PATTern:SCOPe
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:CARRier<Carrier>:HSUPa:EAGCh:PATTern:TYPE
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:CARRier<Carrier>:HSUPa:EAGCh:PATTern:REPetition



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Carrier_.Hsupa_.Eagch_.Pattern.Pattern
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.carrier.hsupa.eagch.pattern.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cell_Carrier_Hsupa_Eagch_Pattern_Execute.rst