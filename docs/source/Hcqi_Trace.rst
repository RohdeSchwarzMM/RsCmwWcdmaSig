Trace
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Hcqi_.Trace.Trace
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.hcqi.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Hcqi_Trace_Carrier.rst