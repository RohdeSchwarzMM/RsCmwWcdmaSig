Conformance
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSDPa:CQI:CONFormance:MODE

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSDPa:CQI:CONFormance:MODE



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Hsdpa_.Cqi_.Conformance.Conformance
	:members:
	:undoc-members:
	:noindex: