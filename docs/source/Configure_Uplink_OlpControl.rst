OlpControl
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:OLPControl:INTerference
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:OLPControl:CVALue

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:UL:OLPControl:INTerference
	CONFigure:WCDMa:SIGNaling<Instance>:UL:OLPControl:CVALue



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Uplink_.OlpControl.OlpControl
	:members:
	:undoc-members:
	:noindex: