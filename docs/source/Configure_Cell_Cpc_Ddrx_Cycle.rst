Cycle
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:DDRX:CYCLe:APATtern
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:DDRX:CYCLe:ITHReshold

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:DDRX:CYCLe:APATtern
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:DDRX:CYCLe:ITHReshold



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Cpc_.Ddrx_.Cycle.Cycle
	:members:
	:undoc-members:
	:noindex: