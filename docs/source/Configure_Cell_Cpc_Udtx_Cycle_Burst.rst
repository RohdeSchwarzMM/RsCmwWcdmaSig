Burst
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:UDTX:CYCLe<Cycle>:BURSt

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:CPC:UDTX:CYCLe<Cycle>:BURSt



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Cpc_.Udtx_.Cycle_.Burst.Burst
	:members:
	:undoc-members:
	:noindex: