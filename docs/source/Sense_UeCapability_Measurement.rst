Measurement
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:WCDMa:SIGNaling<Instance>:UECapability:MEASurement

.. code-block:: python

	SENSe:WCDMa:SIGNaling<Instance>:UECapability:MEASurement



.. autoclass:: RsCmwWcdmaSig.Implementations.Sense_.UeCapability_.Measurement.Measurement
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ueCapability.measurement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UeCapability_Measurement_Cmode.rst