Cswitched
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:WCDMa:SIGNaling<Instance>:CONNection:CSWitched:ATTempt
	single: SENSe:WCDMa:SIGNaling<Instance>:CONNection:CSWitched:REJect

.. code-block:: python

	SENSe:WCDMa:SIGNaling<Instance>:CONNection:CSWitched:ATTempt
	SENSe:WCDMa:SIGNaling<Instance>:CONNection:CSWitched:REJect



.. autoclass:: RsCmwWcdmaSig.Implementations.Sense_.Connection_.Cswitched.Cswitched
	:members:
	:undoc-members:
	:noindex: