Amr
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:VOICe:AMR:NARRow
	single: CONFigure:WCDMa:SIGNaling<Instance>:CONNection:VOICe:AMR:WIDE

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:VOICe:AMR:NARRow
	CONFigure:WCDMa:SIGNaling<Instance>:CONNection:VOICe:AMR:WIDE



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Connection_.Voice_.Amr.Amr
	:members:
	:undoc-members:
	:noindex: