Pdu
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Throughput_.Trace_.Downlink_.Pdu.Pdu
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.throughput.trace.downlink.pdu.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Throughput_Trace_Downlink_Pdu_Current.rst
	Throughput_Trace_Downlink_Pdu_Average.rst