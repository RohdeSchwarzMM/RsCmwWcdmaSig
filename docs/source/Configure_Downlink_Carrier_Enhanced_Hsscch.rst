Hsscch
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:ENHanced:HSSCch:USFRames
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:ENHanced:HSSCch:NUMBer
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:ENHanced:HSSCch:SELection

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:ENHanced:HSSCch:USFRames
	CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:ENHanced:HSSCch:NUMBer
	CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:ENHanced:HSSCch:SELection



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Downlink_.Carrier_.Enhanced_.Hsscch.Hsscch
	:members:
	:undoc-members:
	:noindex: