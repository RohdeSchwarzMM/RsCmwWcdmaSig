Pcpich
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:ENHanced:PCPich:SLEVel

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:DL:CARRier<Carrier>:ENHanced:PCPich:SLEVel



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Downlink_.Carrier_.Enhanced_.Pcpich.Pcpich
	:members:
	:undoc-members:
	:noindex: