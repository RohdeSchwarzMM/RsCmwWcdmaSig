Apattern
----------------------------------------





.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Cpc_.Udtx_.Cycle_.Apattern.Apattern
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.cpc.udtx.cycle.apattern.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cell_Cpc_Udtx_Cycle_Apattern_Tti.rst