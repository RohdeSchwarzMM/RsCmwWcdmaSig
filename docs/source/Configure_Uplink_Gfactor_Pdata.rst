Pdata<PacketData>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Pd8 .. Pd384
	rc = driver.configure.uplink.gfactor.pdata.repcap_packetData_get()
	driver.configure.uplink.gfactor.pdata.repcap_packetData_set(repcap.PacketData.Pd8)



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:GFACtor:PDATa<PacketData>

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:UL:GFACtor:PDATa<PacketData>



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Uplink_.Gfactor_.Pdata.Pdata
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.uplink.gfactor.pdata.clone()