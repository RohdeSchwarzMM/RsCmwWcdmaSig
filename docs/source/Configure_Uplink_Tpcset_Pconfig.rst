Pconfig
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:TPCSet:PCONfig:TSEF
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:TPCSet:PCONfig:TSGH
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:TPCSet:PCONfig:TSSegment
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:TPCSet:PCONfig:PHDown
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:TPCSet:PCONfig:PHUP
	single: CONFigure:WCDMa:SIGNaling<Instance>:UL:TPCSet:PCONfig:DHIB

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:UL:TPCSet:PCONfig:TSEF
	CONFigure:WCDMa:SIGNaling<Instance>:UL:TPCSet:PCONfig:TSGH
	CONFigure:WCDMa:SIGNaling<Instance>:UL:TPCSet:PCONfig:TSSegment
	CONFigure:WCDMa:SIGNaling<Instance>:UL:TPCSet:PCONfig:PHDown
	CONFigure:WCDMa:SIGNaling<Instance>:UL:TPCSet:PCONfig:PHUP
	CONFigure:WCDMa:SIGNaling<Instance>:UL:TPCSet:PCONfig:DHIB



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Uplink_.Tpcset_.Pconfig.Pconfig
	:members:
	:undoc-members:
	:noindex: