Hsupa
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSUPa:TTI
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSUPa:HRVersion
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSUPa:HBDConition
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSUPa:PLPLnonmax
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSUPa:MCCode
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSUPa:ISGRant
	single: CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSUPa:MODulation

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSUPa:TTI
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSUPa:HRVersion
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSUPa:HBDConition
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSUPa:PLPLnonmax
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSUPa:MCCode
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSUPa:ISGRant
	CONFigure:WCDMa:SIGNaling<Instance>:CELL:HSUPa:MODulation



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Cell_.Hsupa.Hsupa
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.hsupa.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cell_Hsupa_Pdu.rst
	Configure_Cell_Hsupa_UeCategory.rst
	Configure_Cell_Hsupa_Eagch.rst
	Configure_Cell_Hsupa_Horder.rst
	Configure_Cell_Hsupa_Etfci.rst
	Configure_Cell_Hsupa_Harq.rst