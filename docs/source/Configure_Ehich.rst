Ehich
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:WCDMa:SIGNaling<Instance>:EHICh:TOUT
	single: CONFigure:WCDMa:SIGNaling<Instance>:EHICh:REPetition
	single: CONFigure:WCDMa:SIGNaling<Instance>:EHICh:MFRames
	single: CONFigure:WCDMa:SIGNaling<Instance>:EHICh:LIMit

.. code-block:: python

	CONFigure:WCDMa:SIGNaling<Instance>:EHICh:TOUT
	CONFigure:WCDMa:SIGNaling<Instance>:EHICh:REPetition
	CONFigure:WCDMa:SIGNaling<Instance>:EHICh:MFRames
	CONFigure:WCDMa:SIGNaling<Instance>:EHICh:LIMit



.. autoclass:: RsCmwWcdmaSig.Implementations.Configure_.Ehich.Ehich
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.ehich.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Ehich_Smode.rst