from ....Internal.Core import Core
from ....Internal.CommandsGroup import CommandsGroup


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class Carrier:
	"""Carrier commands group definition. 15 total commands, 3 Sub-groups, 0 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._base = CommandsGroup("carrier", core, parent)

	@property
	def fsimulator(self):
		"""fsimulator commands group. 4 Sub-classes, 2 commands."""
		if not hasattr(self, '_fsimulator'):
			from .Carrier_.Fsimulator import Fsimulator
			self._fsimulator = Fsimulator(self._core, self._base)
		return self._fsimulator

	@property
	def awgn(self):
		"""awgn commands group. 0 Sub-classes, 3 commands."""
		if not hasattr(self, '_awgn'):
			from .Carrier_.Awgn import Awgn
			self._awgn = Awgn(self._core, self._base)
		return self._awgn

	@property
	def power(self):
		"""power commands group. 1 Sub-classes, 1 commands."""
		if not hasattr(self, '_power'):
			from .Carrier_.Power import Power
			self._power = Power(self._core, self._base)
		return self._power

	def clone(self) -> 'Carrier':
		"""Clones the group by creating new object from it and its whole existing sub-groups
		Also copies all the existing default Repeated Capabilities setting,
		which you can change independently without affecting the original group"""
		new_group = Carrier(self._core, self._base.parent)
		self._base.synchronize_repcaps(new_group)
		return new_group
