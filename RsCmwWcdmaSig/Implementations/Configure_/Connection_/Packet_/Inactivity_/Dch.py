from ......Internal.Core import Core
from ......Internal.CommandsGroup import CommandsGroup


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class Dch:
	"""Dch commands group definition. 6 total commands, 2 Sub-groups, 0 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._base = CommandsGroup("dch", core, parent)

	@property
	def network(self):
		"""network commands group. 0 Sub-classes, 3 commands."""
		if not hasattr(self, '_network'):
			from .Dch_.Network import Network
			self._network = Network(self._core, self._base)
		return self._network

	@property
	def uefDormancy(self):
		"""uefDormancy commands group. 0 Sub-classes, 3 commands."""
		if not hasattr(self, '_uefDormancy'):
			from .Dch_.UefDormancy import UefDormancy
			self._uefDormancy = UefDormancy(self._core, self._base)
		return self._uefDormancy

	def clone(self) -> 'Dch':
		"""Clones the group by creating new object from it and its whole existing sub-groups
		Also copies all the existing default Repeated Capabilities setting,
		which you can change independently without affecting the original group"""
		new_group = Dch(self._core, self._base.parent)
		self._base.synchronize_repcaps(new_group)
		return new_group
